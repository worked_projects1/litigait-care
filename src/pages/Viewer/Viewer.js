/**
 * 
 * Viewer Page
 * 
 */



import React from 'react';
import PDFViewer from '../../components/PDFViewer';

export default function Viewer (props) {
    return <PDFViewer base64={props.match.base64} />
}