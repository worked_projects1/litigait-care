import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    grid: {
        position: 'absolute',
        top: 0,
        bottom: 0,
    },
    twoStep: {
        marginBottom: theme.spacing(2),
        fontSize: '32px',
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: '20px',
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(8),
        marginRight: theme.spacing(8),
        fontSize: '16px',
        textAlign: 'center',
    },
    textMessage: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(8),
        marginRight: theme.spacing(8),
        textAlign: 'center',
        opacity: '0.65',
        fontSize: '16px',
    },
    errorMessage: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(8),
        marginRight: theme.spacing(8),
        textAlign: 'center',
        fontSize: '16px',
        color: '#E07C76',
        fontWeight: 'bold',
    },
    lockIcon: {
        width: '100%',
        height: '40px',
    },
    fieldColor: {
        width: '200px',
        marginTop: '24px',
        '& :after': {
            borderBottomColor: '#2ca01c',
        },
        '& :before': {
            borderBottomColor: '#2ca01c',
        },
        color: 'green !important',
        '& label.Mui-focused': {
            color: '#2ca01c',
        },
        '&.Mui-focused fieldset': {
            borderColor: '#2ca01c',
        },

    },
    btnVerify: {
        fontSize: '20px',
        width: '200px',
        height: '50px',
        margin: '20px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        marginTop: '20px',
        '&:hover': {
            background: '#2ca01c',
        }
    },
    btnVerifyDisabled: {
        fontSize: '20px',
        width: '200px',
        height: '50px',
        margin: '20px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        marginTop: '20px',
        opacity: '0.7',
        '&:hover': {
            background: '#2ca01c',
        }
    },
    linkColor: {
        color: '#0077c5',
        textDecoration: 'none',
        boxShadow: 'none',
        background: 'transparent !important',
        border: 'none',
        textTransform: 'capitalize',
        '&:hover': {
            textDecoration: 'none',
            boxShadow: 'none',
            background: 'transparent',
            border: 'none'
        }
    }
}));


export default useStyles;