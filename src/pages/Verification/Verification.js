/**
 * 
 * Verification
 * 
 */


import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import styles from './styles';
import Button from '@material-ui/core/Button';
import LockIcon from '@material-ui/icons/Lock';
import TextField from '@material-ui/core/TextField';
import { Link } from 'react-router-dom';
import withQueryParams from 'react-router-query-params';
import { compose } from 'redux';

function Verification({ handleVerify, error, sendOtp, resendCount, style, queryParams = {} }) {
    const { TargetLanguageCode } = queryParams;
    const classes = styles();
    const [securityCode, setSecurityCode] = React.useState("");

    const onChange = (e) => {
        setSecurityCode(e.target.value);
    }

    return (
        <Grid
            container
            spacing={0}
            align="center"
            justify="center"
            direction="column"
            style={style}
            className={classes.grid}
        >
            <Grid item >
                <LockIcon className={classes.lockIcon} style={{ height: '60px' }} />
                <Typography variant="h1" gutterBottom className={classes.twoStep}>{TargetLanguageCode === 'es' ? 'Verificación de dos pasos' : TargetLanguageCode === 'vi' ? 'Xác minh hai bước' : 'Two Step Verification'}</Typography>
                <Typography className={classes.instructions} dangerouslySetInnerHTML={{ __html: TargetLanguageCode === 'es' ? `Para mantener sus datos seguros, necesitamos verificar su identidad. <br /> ¡Gracias por su comprensión!` : TargetLanguageCode === 'vi' ? 'Để giữ an toàn cho dữ liệu của bạn, chúng tôi cần xác minh danh tính của bạn. <br /> Cảm ơn bạn đã thông cảm!' : 'In order to keep your data safe we need to verify your identity. <br /> Thank you for your understanding!' }} />

                {error ? <Typography className={classes.errorMessage}>{error}</Typography> : <Typography className={classes.textMessage}>{TargetLanguageCode === 'es' ? 'Se ha enviado un mensaje de texto con un código secreto a su teléfono móvil' : TargetLanguageCode === 'vi' ? 'Một tin nhắn văn bản với mã bí mật đã được gửi đến điện thoại di động của bạn' : 'A text message with secret code has been sent to your mobile phone'}</Typography>}

                <TextField id="filled-basic" label="" variant="filled" className={classes.fieldColor}
                    onChange={onChange}
                    inputProps={{ min: 0, style: { textAlign: 'center', fontSize: '22px' } }}
                />
                <Grid item>
                    {securityCode !== '' ? <Button className={classes.btnVerify}
                        type="button"
                        variant="contained"
                        color="primary"
                        onClick={() => handleVerify(securityCode)}>
                        {TargetLanguageCode === 'es' ? 'Verificar' : TargetLanguageCode === 'vi' ? 'Xác nhận' : 'Verify'}
                    </Button> : <Button className={classes.btnVerifyDisabled}
                            type="button"
                            variant="contained"
                            color="primary">
                            {TargetLanguageCode === 'es' ? 'Verificar' : TargetLanguageCode === 'vi' ? 'Xác nhận' : 'Verify'}
                    </Button>}

                </Grid>

                {resendCount < 2 ? <Grid item xs>
                    <Button className={classes.linkColor}
                        type="button"
                        variant="contained"
                        onClick={sendOtp}
                        color="primary">
                        {TargetLanguageCode === 'es' ? 'Reenviar OTP' : TargetLanguageCode === 'vi' ? 'Gửi lại OTP' : 'Resend OTP'}
                    </Button>
                </Grid> : null}
            </Grid>
        </Grid>
    )

}

export default compose(
    withQueryParams()
)(Verification);