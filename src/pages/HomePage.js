/**
 * 
 * Home Page
 * 
 */


import React, { useEffect, useRef, useState } from 'react'
import styled from 'styled-components';
import LockIcon from '@material-ui/icons/Lock';
import ClipLoader from 'react-spinners/ClipLoader';
import './page.css';
import api from '../Utils/api';
import withQueryParams from 'react-router-query-params';
import { compose } from 'redux';

const H1 = styled.h1`
  font-size: 2rem;
  font-weight: 400;
  margin-top: 0px;
  margin-bottom: 0.5em;
`;

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 16px;
  min-width: 0;
  min-width: 100%;
`;

const CustomWrapper = styled(Wrapper)`
  justify-content: center;
  text-align: center;
`;

const HelperText = styled.p`
  opacity: .5;
  line-height: 1.45;
`;

const CharacterInputWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 1.5em -.25em;
`;

const CharacterInput = styled.input.attrs({ type: 'text' })`
  min-height: 3em;
  font-size: 1.5em;
  font-weight: 500;
  background: rgba(0, 12, 63, 0.05);
  min-width: 0;
  border: none;
  text-align: center;
  border-bottom: 2px solid #000C3F;
  margin: 0 .25em;
  color: #000C3F;
  padding-left: .5em;
  padding-right: .5em;
  transition: border-color 0.2s ease-in-out;

  &::placeholder {
    color: rgba(255, 255, 255, 0.5);
    opacity: 0.5;
  }

  &:focus {
    border-color: #2ca01c;
    outline: none;
  }

  :disabled,
  &[disabled] {
    opacity: 0.5;
    cursor: default;
    pointer-events: none;
  }

  &[type='number'] {
    width: 100%;
  }

  @media (max-width: 725px) {
    font-size: 3vw;
  }
`;

const TitleIcon = styled.div`
  margin-bottom: 1em;

  span {
    font-size: 3em;
    color: #000c3f;
    opacity: .25;
  }
`;

const PinInput = ({ value, onChange, onComplete, disabled }) => {
  const refs = [useRef(), useRef(), useRef(), useRef(), useRef(), useRef(), useRef(), useRef()];

  const current = value.length < refs.length ? refs[value.length].current : undefined;

  useEffect(() => {
    if (current && !disabled) {
      current.focus();
    }
    if (value.length === 8) {
      onComplete(value);
    }
  }, [current, disabled, value, onComplete]);

  const handleFocus = i => () => {
    if (value.length > i) {
      onChange(value.substring(0, i));
    } else if (value.length < i) {
      refs[value.length].current.focus();
    }
  };

  const handleChange = i => e => {
    const nextValue = e.target.value.toUpperCase().replace(/[^A-Z0-9]+/g, '');
    onChange(value + nextValue.slice(0, 8 - value.length));
  };

  const handleKeydown = i => e => {
    const key = e.keyCode || e.charCode;

    if (key === 8 || key === 46) {
      refs[i].current.focus();
      if (value.length > 0) {
        onChange(value.slice(0, i - 1));
      }
    }
  };

  return (
    <CharacterInputWrapper>
      {refs.map((ref, i) =>
        <CharacterInput
          key={i}
          ref={ref}
          autoFocus={i === 0}
          onFocus={handleFocus(i)}
          value={value[i] || ''}
          onChange={handleChange(i)}
          onKeyDown={handleKeydown(i)}
          disabled={disabled}
        />
      )}
    </CharacterInputWrapper>
  );
};

function HomePage(props) {
  const { history } = props;
  const [value, setValue] = useState('');
  const [error, setError] = useState();
  const [busy, setBusy] = useState();

  useEffect(() => {
    if (error) {
      const timeout = setTimeout(() => {
        setError();
      }, 2000);

      return () => clearTimeout(timeout);
    }
  }, [error]);


  const logIn = async value => {
    setBusy(true);

    await api.get(`forms/get-questions-by-otp?otp_code=${value}`).then(() => {
      history.push(`/questionnaire/${value}`);
    }).catch(() => {
      setError('PIN is not valid, please try again.');
    }).finally(() => {
      setValue('');
      setBusy(false);
    });

  };

  return (
    <div className="Page Disclaimer" style={{ minHeight: `${Math.max(window.innerHeight, document.body.clientHeight) - 50}px` }}>
      <CustomWrapper>
        <TitleIcon>
          <LockIcon style={{ width: '100%', height: '70px' }}>lock</LockIcon>
        </TitleIcon>
        <H1>Please enter your PIN</H1>
        <PinInput value={value} onChange={setValue} onComplete={logIn} disabled={busy} />
        <div style={{ textAlign: 'center' }}>
          {busy && <ClipLoader color={"#2ca01c"} />}
          {!busy && <span style={{ color: '#b6a621', fontWeight: 500 }}>{error}</span>}
        </div>
        <HelperText>You can find the PIN in the text message you have received from the lawsuit.</HelperText>
      </CustomWrapper>
    </div>
  );
}


export default compose(
  withQueryParams()
)(HomePage);