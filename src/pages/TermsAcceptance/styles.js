import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    questionnaire: {
        fontSize: '22px',
        marginBottom: theme.spacing(2),
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(8),
        marginRight: theme.spacing(8),
        textAlign: 'center',
    },
    thankyou: {
        marginBottom: theme.spacing(1),
        fontSize: '22px',
        textAlign: 'center',
    },
    checkIcon: {
        width: '100%',
        height: '100px',
        color: '#2ca01c',
    }
}));


export default useStyles;