/**
 * 
 * Terms Acceptance 
 * 
 */

import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import styles from './styles';
import api from '../../Utils/api';
import styled from 'styled-components';
import Introduction from '../Introduction/Introduction';
import ClipLoader from 'react-spinners/ClipLoader';
import Terms from '../../components/Terms';
import CheckBoxRoundedIcon from '@material-ui/icons/CheckBoxRounded';


const ClipWrapper = styled.div`
  text-align: center;
  margin-top: 100px
`;


function TermsAcceptance(props) {

    const { match } = props;
    const classes = styles();
    //const [error, setError] = useState(false);
    //const [success, setSuccess] = useState(false);
    const [busy, setBusy] = useState(false);
    const [page, setPage] = useState(0);
    const [record, setRecord] = useState(false);
    const [pendingAcceptance, setPendingAcceptance] = useState(false);

    const setAcceptance = (record) => {
        if (match.path.indexOf('hipaa-terms') !== -1) {
            setPendingAcceptance("HIPAA Form");
        } else {
            setPendingAcceptance("Fee Agreement");
        }
    }

    useEffect(() => {
        setBusy(true);
        api.post((match.path.indexOf('hipaa-terms') !== -1) ? `hipaa-terms/get-terms-by-client-id` : 'rest/fee-terms/get-terms-by-clientid-caseid', Object.assign({}, { client_id: match.params.clientId, case_id: match.params.caseId })).then((response) => {
            setRecord(response && response.data);
            setAcceptance(response && response.data)
            if (response && response.data && (response.data.fee_acceptance_status || response.data.hipaa_acceptance_status)) {
                setPage(2);
            }
            setBusy(false);
        }).catch((error) => {
            // history.push(`/`);
            //setError(error);
            setBusy(false);
        }).finally(() => {
            // setBusy(false);
        });
    }, []);

    const IntroductionPage = () => {
        return (<Introduction handleContinue={handleContinue}
            title={"Welcome To Client Online Registration"}
            content={`Your attorney has sent you ` + pendingAcceptance + `. Please review and accept them at your earliest convenience.`} />
        );
    }

    const ThankYouPage = () => {
        return (
            <Grid container justify="center" spacing={2}>
                <Grid item xs={12} style={{ marginTop: '20px' }}>
                    <CheckBoxRoundedIcon className={classes.checkIcon} />
                </Grid>
                <Grid>
                    <Typography variant="subtitle2" className={classes.thankyou}><b>Thank You</b></Typography>
                    <Typography className={classes.instructions}>The Terms were updated successfully. The Lawyer may call you for further clarification.</Typography>
                </Grid>
            </Grid>
        );
    }

    const handleHIPAAPage = (page) => {
        switch (page) {
            case 0:
                return IntroductionPage();
            case 1:
                return (<Terms handleClick={handleClick}
                    title={"HIPAA Form"}
                    value={"hipaa_acceptance_status"}
                    content={record && record.terms_text} />
                );
            case 2:
                return ThankYouPage();
            default:
                break;
        }

    }

    const handleFeePage = (page) => {
        switch (page) {
            case 0:
                return IntroductionPage();
            case 1:
                return (<Terms handleClick={handleClick}
                    title={"Fee Agreement"}
                    value={"fee_acceptance_status"}
                    content={record && record.terms_text} />
                );
            case 2:
                return ThankYouPage();
            default:
                break;
        }

    }

    const handleClick = async (type, title, value) => {
        setPage(page => page + 1);
        if (type === 'accept') {
            setBusy(true);
            await api.put(`clients/accept-terms/${match.params.clientId}`, Object.assign({}, { [value]: true, case_id: match.params.caseId })).then((response) => {
                //setSuccess(true);
            }).catch((error) => {
                //setError(error);
            }).finally(() => {
                setBusy(false);
            });
        }
    }

    const handleContinue = () => {
        setPage(page => page + 1);
    }

    if (busy) {
        return <ClipWrapper>
            <ClipLoader color={"#2ca01c"} />
        </ClipWrapper>
    }

    return (
        <Grid container>
            {(match.path.indexOf('hipaa-terms') !== -1) ? handleHIPAAPage(page) : handleFeePage(page)}
        </Grid>
    );
}

export default TermsAcceptance;
