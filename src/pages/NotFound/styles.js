import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    pageNotFound: {
        textAlign: 'center',
        fontWeight: 'bold',
    },
    label: {
        textAlign: 'center',
        marginTop: '40px',
        fontSize: '26px',
    },
    grid: {
        position: 'absolute',
        height: '100%',
    }
}));


export default useStyles;