/**
 * 
 * Not Found Page
 * 
 */

import React from 'react';
import styles from './styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

function NotFound() {
    const classes = styles();
    return (
        <Grid container
            justify="center"
            alignItems="center"
            className={classes.grid}
        >
            <div style={{ margin: '0px 30px' }}>
                <Typography variant="h3" className={classes.pageNotFound}>Page Not Found</Typography>
                <Typography className={classes.label}>Oops! Looks like the page does not exist.</Typography>
            </div>
        </Grid>
    );
}

export default NotFound;