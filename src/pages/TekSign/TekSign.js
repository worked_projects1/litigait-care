

/**
 * 
 * TekSign Page
 */

import React, { useEffect, useState } from 'react';
import { Grid, Button, Typography } from '@material-ui/core';
import styled from 'styled-components';
import ClipLoader from 'react-spinners/ClipLoader';
import Verification from '../Verification/Verification';
import store2 from 'store2';
import api from '../../Utils/api';
import styles from './styles';
import Snackbar from '../../components/Snackbar';
import CheckBoxRoundedIcon from '@material-ui/icons/CheckBoxRounded';
import Document from '../Document/Document';
import SubdirectoryArrowRightIcon from '@material-ui/icons/SubdirectoryArrowRight';
import axios from 'axios';
import { CreatePDF } from '../../components/PDFMaker/utils';
import Definition from './Definition';
import moment from 'moment';
import lodash from 'lodash';
import { compose } from 'redux';
import withQueryParams from 'react-router-query-params';
import ModalDialog from '../../components/ModalDialog';
import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import TextField from '@material-ui/core/TextField';
import ButtonSpinner from '../../components/ButtonSpinner';

const ClipWrapper = styled.div`
  text-align: center;
  margin-top: 100px
`;

function TekSign(props) {

    const { history, queryParams = {} } = props;
    const { TargetLanguageCode } = queryParams;
    const classes = styles();
    const [busy, setBusy] = useState(false);
    const [error, setError] = useState(false);
    const [showVerification, setShowVerification] = useState(false);
    const [resendCount, setResendCount] = useState(0);
    const [questions, setQuestions] = useState(false);
    const [signature, setSignature] = useState(false);
    const [showSuccess, setshowSuccess] = useState(0);
    const [showModel, setshowModel] = useState(false);
    const [value, setValue] = useState('');
    const [success, setSuccess] = useState(false);
    const [spinner, setSpinner] = useState(false);
    const theme = useTheme();
    const md = useMediaQuery(theme.breakpoints.up('md'));
    const sm = useMediaQuery(theme.breakpoints.up('sm'));
    const record = questions && questions.length > 0 && questions[0];
    const { case_title, county, document_type, client_name, state, federal, case_plaintiff_name, case_defendant_name, case_number, set_number } = record;
    const questionsList = document_type && document_type.toLowerCase() === 'odd' ? questions && lodash.groupBy(questions, 'question_category_id') : questions && lodash.groupBy(questions, 'question_number_text') || {};

    useEffect(() => {
        const otp = store2.get('otp');
        const token = store2.get('token');
        if (otp && token === props.match.params.token)
            handleVerification(otp)
        else
            setShowVerification(true);
    }, []);

    const FetchQuestions = async () => {
        setBusy(true);
        await api.get(`rest/forms/get-laywer-response-data-by-otp?otp_code=${props.match.params.token}`).then((response) => {
            setQuestions(response.data);
            setBusy(false);
        }).catch((error) => {
            history.push(`/`);
            setError(error);
        });
    }

    const sendOtp = async () => {
        setResendCount(resendCount => resendCount + 1);
        setBusy(true);
        await api.post(`clients/sent-otp`, Object.assign({}, { otp_code: props.match.params.token, TargetLanguageCode })).then((response) => {
            setShowVerification(true);
            setBusy(false)
        }).catch((error) => {
            history.push(`/`);
            setError(error);
        });
    }

    const handleVerification = async (securityCode) => {
        setBusy(true);
        await api.post(`clients/validate-otp`, Object.assign({}, { otp_code: props.match.params.token, otp_secret: securityCode })).then((r) => {
            store2.set('otp', securityCode);
            store2.set('token', props.match.params.token);
            setShowVerification(false);
            FetchQuestions();
        }).catch((error) => {
            const Err = error && error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error === `Invalid OTP` ? TargetLanguageCode === 'es' ? 'El código ingresado no es válido. Inténtalo de nuevo.' : TargetLanguageCode === 'vi' ? `Mã đã nhập không hợp lệ. Vui lòng thử lại.` : `Entered code is not valid. Please try again.` : error.response.data.error : "Entered code is not valid. Please try again.";
            setError(Err);
        }).finally(() => {
            setBusy(false);
        });
    }

    const handleDocumentSubmit = async (data) => {

        if (data.client_signature && data.Terms) {
            setBusy(true);
            CreatePDF({
                questions, docDefinition: Definition({ questions, signatureData: data.client_signature, case_title, county: data.County, document_type, client_name, TargetLanguageCode, state, federal, case_plaintiff_name, case_defendant_name, case_number, set_number }), type: '_blob', callback: async (blob) => {
                    await api.get(`rest/client-signature/get-signature-upload-url-by-otp?otp_code=${props.match.params.token}&file_name=output_${new Date().getUTCMilliseconds()}.pdf`).then(async (response) => {
                        const res = response.data;
                        if (res && res.data && res.data.uploadURL && res.data.s3_file_key) {
                            await axios.put(res.data.uploadURL, blob, {
                                headers: {
                                    Accept: 'application/json',
                                    'Content-Type': 'application/pdf',
                                },
                            }).then(async () => {
                                const client_signature_data = JSON.stringify(Object.assign({}, data, { created_date: moment().format('MM/DD/YYYY') }));
                                await api.put(`rest/client-signature/save-signature-in-s3`, Object.assign({}, { otp_code: props.match.params.token, client_signature_s3_key: res.data.s3_file_key, client_signature_data })).then(async (client_response) => {
                                    setshowSuccess(true);
                                }).catch((error) => {
                                    const signatureError = TargetLanguageCode == 'es' ? `No se pudo actualizar la firma` : TargetLanguageCode == 'vi' ? `Không cập nhật được chữ ký` : 'Failed to update signature';
                                    setError(error.response && error.response.data && error.response.data.error || signatureError);
                                }).finally(() => {
                                    setBusy(false);
                                });
                            }).catch(Err => {
                                const errorMsg = TargetLanguageCode == 'es' ? `No se pudo cargar el documento` : TargetLanguageCode == 'vi' ? `Không tải lên được tài liệu` : 'Failed to Upload Document';
                                setError(errorMsg);
                            }).finally(() => {
                                setBusy(false);
                            });
                        }
                    }).catch((error) => {
                        const errorMsg = TargetLanguageCode == 'es' ? `No se pudo cargar el documento` : TargetLanguageCode == 'vi' ? `Không tải lên được tài liệu` : 'Failed to Upload Document';
                        setError(error.response && error.response.data && error.response.data.error || errorMsg);
                    }).finally(() => {
                        setBusy(false);
                    });
                }
            })

        } else if (!data.client_signature && !data.Terms) {
            setError('Please draw a signature and agree to use electronic records and signature');
        } else if (!data.client_signature) {
            setError('Please draw a signature');
        } else if (!data.Terms) {
            setError('Please agree to use electronic records and signature');
        } else {
            setError('Please provide all details');
        }

    }

    const handleCorrectionSubmit = async () => {
        if (value) {
            await api.post('/rest/client-signature/teksign-correction', Object.assign({}, { otp_code: props.match.params.token, message: value })).then((response) => {
                setshowModel(false);
                setSpinner(true);
                setTimeout(() => {
                    setSpinner(false);
                    const successMessage = TargetLanguageCode === 'es' ? 'Mensaje enviado al letrado' : TargetLanguageCode === 'vi' ? `Tin nhắn gửi đến Luật sư` : `Message sent to Lawyer`;
                    setSuccess(successMessage)
                }, 2000);
                setValue('');
            }).catch(err => {
                setshowModel(false);
                setSpinner(true);
                setTimeout(() => {
                    setSpinner(false);
                    const errorMessage = TargetLanguageCode === 'es' ? 'No se pudo enviar un mensaje' : TargetLanguageCode === 'vi' ? `Không gửi được tin nhắn` : `Failed to send Message`;
                    setError(errorMessage)
                }, 2000);
                setValue('');
            }).finally(() => {
                setBusy(false);
            });
        }
    }

    if (busy) {
        return <ClipWrapper>
            <ClipLoader color={"#2ca01c"} />
        </ClipWrapper>
    }

    return (<Grid container item spacing={3} justify="center" style={{ margin: '0px', width: '100%' }}>
        <Grid style={{ maxWidth: '600px', width: '100%' }}>
            {showSuccess || (record && record.client_signature_exist) ?
                <Grid item xs={12}>
                    <Grid container justify="center" spacing={2}>
                        <Grid item xs={12}>
                            <CheckBoxRoundedIcon className={classes.checkIcon} style={{ width: '100%', height: '100px' }} />
                        </Grid>
                        <Grid item xs={12} className={classes.thankyou}>
                            <Typography variant="subtitle2"><b>{TargetLanguageCode === 'es' ? 'Gracias' : TargetLanguageCode === 'vi' ? 'Cảm ơn' : 'Thank You'}</b></Typography>
                        </Grid>
                        <Grid item xs={12} className={classes.instructions}>
                            <Typography>{TargetLanguageCode === 'es' ? `La firma se envió correctamente.` : TargetLanguageCode === 'vi' ? `Chữ ký đã được gửi thành công.` : `The Signature was submitted successfully.`}</Typography>
                        </Grid>
                    </Grid>
                </Grid>
                : showVerification ? <Verification
                    style={{ position: 'inherit', marginTop: '100px' }}
                    handleVerify={handleVerification}
                    error={error}
                    sendOtp={sendOtp}
                    resendCount={resendCount} /> : signature && questions ?
                    <Document
                        style={{ marginTop: '30px' }}
                        handleDocumentSubmit={handleDocumentSubmit}
                        handleBackPage={() => {
                            setSignature(false);
                        }}
                        onError={(e) => setError(e)}
                        docDefinition={Definition}
                        records={questions}
                    />
                    : questions ? <Grid item xs={12} style={{ padding: '20px', marginTop: '20px' }}>
                        <Grid container direction="column" spacing={2} style={sm ? { marginTop: '50px' } : { marginTop: '100px' }}>
                            <Grid item xs={12} className={classes.header} style={!sm && { textAlign: 'center' } || {}}>
                                <Button
                                    className={classes.button}
                                    type="submit"
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setSignature(true)}>
                                    {TargetLanguageCode === 'es' ? `Confirmar y firmar` : TargetLanguageCode === 'vi' ? `Xác nhận và ký tên` : `Confirm & Sign`}
                                </Button>
                                <Button
                                    type="button"
                                    className={classes.button}
                                    variant="contained"
                                    color="primary"
                                    onClick={() => setshowModel(true)}>
                                    {spinner && <ButtonSpinner /> || (TargetLanguageCode === 'es' ? `Necesita corrección` : TargetLanguageCode === 'vi' ? `Cần sửa chữa` : `Needs Correction`)}
                                </Button>
                            </Grid>
                            {questions && questions.length > 0 && questionsList && Object.keys(questionsList).length > 0 ? (Object.keys(questionsList) || []).map((key, index) => {

                                let isConsultation = questionsList && questionsList[key].length > 0 && questionsList[key][0] && questionsList[key][0]['is_consultation_set'] || false;
                                let consultationSet = isConsultation && isConsultation == 1 && lodash.groupBy(questionsList[key], 'consultation_set_no') || [];

                                const categoryQuestions = document_type && document_type.toLowerCase() === 'odd' && questionsList && questionsList[key] && questionsList[key][0] && questionsList[key][0]['question_category'] || false;

                                return <React.Fragment key={index}>
                                    {categoryQuestions ? <Typography style={{ fontWeight: 700, paddingLeft: '8px', textDecoration: 'underline', textTransform: 'capitalize', fontSize: '17px', paddingBottom: '8px' }}>{`Category - ${categoryQuestions.toLowerCase()}`}</Typography> : null}
                                    {questionsList[key] && questionsList[key].length > 0 && questionsList[key][0]['question_section_text'] ?
                                        <Typography variant="subtitle2" style={{ padding: '12px' }}>
                                            <b>{questionsList[key][0]['question_number_text']}. {questionsList[key][0]['document_type'] == 'FROGS' ? (questionsList[key][0]['question_text']).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : questionsList[key][0]['question_text']}</b>
                                        </Typography>
                                        : null}
                                    <Grid container>
                                        {(!isConsultation && questionsList[key] && questionsList[key].length > 0 && (questionsList[key] || []).map((ques, formIndex) => (<React.Fragment key={formIndex}>
                                            <Grid item xs={12} className={classes.question}>
                                                {ques.question_section ? <b>{ques.question_section} &nbsp;&nbsp; {(ques.question_section_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>
                                                    :
                                                    <b>{ques.question_number_text}. &nbsp;&nbsp; {(ques.question_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>}
                                            </Grid>
                                            <Grid item xs={12} className={classes.response}>
                                                <Grid container direction="row">
                                                    <SubdirectoryArrowRightIcon style={{ marginBottom: '3px', marginRight: '3px' }} />
                                                    <Grid className={classes.text}>
                                                        {ques.lawyer_response_text && ques.lawyer_response_text.toString().replace(/<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g, '').split('\n').filter(x => x).map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.lineResponse}>{line}</p>)}
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </React.Fragment>)) || null)}
                                        {isConsultation && (Object.keys(consultationSet || []).map(key => (
                                            <React.Fragment>
                                                {consultationSet && consultationSet[key] && consultationSet[key].length > 0 && isConsultation ? <Typography variant="subtitle2" style={{ padding: '8px' }}><b>{consultationSet[key] && consultationSet[key][0].document_type == 'IDC' ? `Person ` : `Consultation `}{consultationSet[key][0].consultation_set_no}</b></Typography> : null}
                                                {consultationSet && consultationSet[key] && consultationSet[key].length > 0 && consultationSet[key].map((ques, formIndex) =>
                                                    <React.Fragment key={formIndex}>

                                                        <Grid item xs={12} className={classes.question}>
                                                            {ques.question_section ? <b>{ques.question_section} &nbsp;&nbsp; {(ques.question_section_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>
                                                                :
                                                                <b>{ques.question_number_text}. &nbsp;&nbsp; {(ques.question_text).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <span key={pos} className={classes.line}>{line}</span>)}</b>}
                                                        </Grid>
                                                        <Grid item xs={12} className={classes.response}>
                                                            <Grid container direction="row">
                                                                <SubdirectoryArrowRightIcon style={{ marginBottom: '3px', marginRight: '3px' }} />
                                                                <Grid className={classes.text}>
                                                                    {ques.lawyer_response_text && ques.lawyer_response_text.toString().replace(/<[^>]*(>|$)|&nbsp;|&zwnj;|&raquo;|&laquo;|&rdquo;|&ldquo;|&amp;|&gt;/g, '').split('\n').filter(x => x).map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.lineResponse}>{line}</p>)}
                                                                </Grid>
                                                            </Grid>
                                                        </Grid>
                                                    </React.Fragment>
                                                )}
                                            </React.Fragment>
                                        )) || null)}
                                    </Grid>
                                </React.Fragment>
                            }) : <Grid container justify="center" className={classes.notFound}>
                                No Questions Found.
                            </Grid>}
                        </Grid>
                    </Grid> : null}
            <ModalDialog
                show={showModel}
                style={!md && { justifyContent: 'center' } || {}}
                onClose={() => setshowModel(false)}
                handleSubmit={handleCorrectionSubmit}
                disabled={value && value ? false : true}
                Label={TargetLanguageCode === 'es' ? `Enviar` : TargetLanguageCode === 'vi' ? `Gửi` : `Send`}
                CancelLabel={TargetLanguageCode === 'es' ? `Hủy bỏ` : TargetLanguageCode === 'vi' ? `Cancelar` : `Cancel`}
                heading={() =>
                    <Grid container direction="column">
                        <Grid item xs={12} style={{ paddingTop: '10px' }}>
                            <Typography className={classes.title}>{TargetLanguageCode === 'es' ? `Necesita corrección` : TargetLanguageCode === 'vi' ? `Cần sửa chữa` : `Needs Correction`}</Typography>
                        </Grid>
                        <Grid item xs={12} className={classes.inputField}>
                            <TextField
                                className={classes.fieldColor}
                                multiline
                                name='tekSign'
                                placeholder={TargetLanguageCode === 'es' ? `Proporcione los números de las preguntas y los detalles de las correcciones necesarias. Su abogado hará las correcciones y enviará este formulario para su firma.` : TargetLanguageCode === 'vi' ? `Vui lòng cung cấp số câu hỏi và chi tiết về các chỉnh sửa được yêu cầu. Luật sư của bạn sẽ thực hiện các chỉnh sửa và gửi lại biểu mẫu này để có chữ ký của bạn.` : `Please give question numbers and details of corrections required. Your attorney will make the corrections and resend this form for your signature.`}
                                type='text'
                                rows={6}
                                rowsMax={8}
                                fullWidth
                                onChange={(e) => setValue(e.target.value)}
                                value={value || ''}
                                autoFocus={true}
                                InputProps={{
                                    classes: {
                                        input: classes.textSize,
                                        padding: 0,
                                    },

                                }}
                            />
                        </Grid>
                    </Grid>
                }
            />
        </Grid>
        <Snackbar show={error ? true : false} text={error} severity={'error'} handleClose={() => setError(false)} />
        <Snackbar show={success ? true : false} text={success} severity={'success'} handleClose={() => setSuccess(false)} />
    </Grid>)
}

export default compose(
    withQueryParams()
)(TekSign);