import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    question: {
        padding: '8px',
        fontSize: '16px'
    },
    response: {
        paddingLeft: '8px',
        paddingRight: '8px',
        paddingBottom: '25px'
    },
    text: {
        paddingTop: '4px',
        fontSize: '15px',
        '& > span': {
            '& > p': {
                margin: '0px !important'
            }
        }
    },
    notFound: {
        marginTop: '15px'
    },
    button: {
        fontSize: '14px',
        padding: '5px 20px',
        marginTop: '10px',
        marginRight: '15px',
        marginBottom: '10px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        '&:hover': {
            backgroundColor: '#2ca01c'
        },
        [theme.breakpoints.down('xs')]: {
            padding: '5px 10px'
        }
    },
    thankyou: {
        padding: '0px !important',
        textAlign: 'center',
        '& h6': {
            fontSize: '22px'
        }
    },
    checkIcon: {
        width: '100%',
        height: '100px',
        color: '#2ca01c',
    },
    instructions: {
        padding: '0px !important',
        '& p': {
            marginTop: theme.spacing(1),
            marginBottom: theme.spacing(1),
            marginLeft: theme.spacing(8),
            marginRight: theme.spacing(8),
            textAlign: 'center'
        }
    },
    header: {
        position: 'fixed',
        width: '576px',
        background: 'white',
        paddingTop: '28px !important',
        top: '0'
    },
    line: {
        margin: '4px'
    },
    lineResponse: {
        marginTop: '10px'
    },
    title: {
        fontWeight: 'bold',
        paddingBottom: '10px'
    },
    textSize: {
        fontSize: '14px',
        padding: 0,
    },
    inputField: {
        display: 'flex', 
        flexWrap: 'wrap', 
        justifyContent: 'center' 
    },
    fieldColor: {
        background: 'rgba(0, 0, 0, 0.09)',
        color: 'black !important',
        height: '100px',
        overflowY: 'auto',
        padding: '10px',
        fontSize: '12px',
        borderBottom: '2px solid #2ca01c',
        cursor: 'pointer'
    }

}));
export default useStyles;