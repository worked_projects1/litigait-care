/**
 * 
 * 
 * Definition
 * 
 */


import moment from 'moment';

/**
 * 
 * @param {Object} params 
 */
export default function Definition(params = {}) {

    const { questions, signatureData, case_title, county, document_type, client_name, TargetLanguageCode, state, federal, case_plaintiff_name, case_defendant_name, case_number, set_number } = params;
    // const questionsDef = questions && questions.length > 0 && questions.reduce((arr, ques) => {
    //     arr.push([{
    //         text: `${ques.question_number_text}. ${ques.question_text.toString().split('\n').map(q => q).join(' ')}`,
    //         style: 'questions'
    //     }, {
    //         text: `${ques.lawyer_response_text}`,
    //         style: 'response'
    //     }]);
    //     return arr;
    // }, [{
    //     text: 'Questionnaire for the lawsuit',
    //     bold: true,
    //     decoration: 'underline',
    //     fontSize: 18,
    //     lineHeight: 1.2,
    // }]) || [];

    const questionsDef = [];
    return state === 'TX' ? {
        content: questionsDef.concat([
            {
                text: 'VERIFICATION',
                style: 'title',
                alignment: 'center'
            },
            {
                text: [
                    { text: `&nbsp;`, color: '#FFFFFF' },
                    `I, `, { text: `${client_name || ''}, `, bold: true, fontSize: 15 },
                    `am a party in the case at bar and I have read the foregoing `,
                    { text: `${document_type && getDocumentType(document_type) || ''}`, bold: true, fontSize: 15 },
                    ` as propounded by the opposing party named herein, and the answers are true and correct to the best of my knowledge and belief, exclusive of any answers that are based on information obtained from another person (which are so stated), and any answers about persons with knowledge of relevant facts, trial witnesses, and legal contentions.`

                ],
                style: 'body'
            },
            {
                style: 'tableStyle',
                table: {
                    headerRows: 1,
                    widths: [250, 250],
                    heights: [150, 150],
                    body: [
                        [
                            [signatureData ? [
                                {
                                    image: signatureData,
                                    width: 120,
                                    height: 70,
                                    alignment: 'left'
                                },
                                {
                                    text: `________________________________`,
                                    bold: true,
                                    alignment: 'left'
                                },
                            ] : [{
                                text: `Signature goes here`,
                                bold: true,
                                color: '#3867BB',
                                alignment: 'left',
                                lineHeight: 0.5,
                                fontSize: 15,
                                margin: [0, 61, 0, 0]
                            },
                            {
                                text: `________________________________`,
                                bold: true,
                                alignment: 'left'
                            }],
                            {
                                text: `${client_name || ''}`,
                                bold: true,
                                fontSize: 15,
                                alignment: 'left'
                            }
                            ],
                            [{
                                text: `${moment().format('MM/DD/YYYY')}`,
                                alignment: 'left',
                                bold: true,
                                lineHeight: 0.5,
                                margin: [50, 61, 0, 0]
                            },
                            {
                                text: `________________________`,
                                alignment: 'left',
                                bold: true,
                                margin: [50, 0, 0, 0],
                            },
                            {
                                text: `Date`,
                                alignment: 'left',
                                bold: true,
                                margin: [50, 0, 0, 0],
                            }],
                        ]
                    ]
                },
                layout: 'noBorders'
            },
        ]),
        styles: {
            body: {
                lineHeight: 1.4,
                fontSize: 15,
                margin: [10, 40, 0, 0]
            },
            columnStyle: {
                fontSize: 15,
                margin: [10, 40, 0, 0]
            },
            title: {
                fontSize: 18,
                bold: true,
                alignment: 'justify',
                decoration: 'underline',
                lineHeight: 1.3,
                margin: [0, 45, 0, 0]
            },
            tableStyle: {
                margin: [10, 20, 0, 0],
                width: '*',
                fontSize: 15,
            }
        }
    } : state === 'IA' ? {
        content: questionsDef.concat([
            {
                text: 'VERIFICATION',
                style: 'title',
                alignment: 'center'
            },
            {
                text: `I certify under penalty of perjury and pursuant to the laws of the State of Iowa that the answers contained herein are true and correct to the best of my knowledge.`
                ,
                style: 'body'
            },
            {
                style: 'tableStyle',
                table: {
                    headerRows: 1,
                    widths: [280, 240],
                    heights: [150, 150],
                    body: [
                        [
                            [
                                {
                                    text: `${moment().format('MM/DD/YYYY')}`,
                                    alignment: 'left',
                                    bold: true,
                                    lineHeight: 0.5,
                                    margin: [0, 61, 0, 0]
                                },
                                {
                                    text: `_____________________________________`,
                                    alignment: 'left',
                                    bold: true,
                                    margin: [0, 0, 0, 0],
                                },
                                {
                                    text: `Date`,
                                    alignment: 'left',
                                    bold: true,
                                    margin: [0, 0, 0, 0],
                                }
                            ],
                            [signatureData ?
                                [
                                    {
                                        image: signatureData,
                                        width: 120,
                                        height: 70,
                                        alignment: 'left'
                                    },
                                    {
                                        text: `________________________`,
                                        bold: true,
                                        alignment: 'left'
                                    },
                                ] : [
                                    {
                                        text: `Signature goes here`,
                                        bold: true,
                                        color: '#3867BB',
                                        alignment: 'left',
                                        lineHeight: 0.5,
                                        fontSize: 15,
                                        margin: [0, 61, 0, 0]
                                    },
                                    {
                                        text: `________________________`,
                                        bold: true,
                                        alignment: 'left'
                                    }
                                ],
                            {
                                text: `${client_name || ''}`,
                                bold: true,
                                fontSize: 15,
                                alignment: 'left'
                            }
                            ]
                        ]
                    ]
                },
                layout: 'noBorders'
            },
        ]),
        styles: {
            body: {
                lineHeight: 1.4,
                fontSize: 15,
                margin: [10, 40, 0, 0]
            },
            columnStyle: {
                fontSize: 15,
                margin: [10, 40, 0, 0]
            },
            title: {
                fontSize: 18,
                bold: true,
                alignment: 'justify',
                decoration: 'underline',
                lineHeight: 1.3,
                margin: [0, 45, 0, 0]
            },
            tableStyle: {
                margin: [10, 20, 0, 0],
                width: '*',
                fontSize: 15,
            }
        }
    } : state === 'NY' ? {
        content: [
            {
                style: 'tableStyle',
                table: {
                    widths: [300, 50, 135],
                    heights: 15,
                    body: [
                        [
                            {
                                text: 'CIVIL COURT OF THE CITY OF NEW YORK',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [

                            {
                                text: `COUNTY OF ${county || ''}`,
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },

                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: `${case_plaintiff_name || ''}`,
                                bold: true,
                                border: [false, true, false, false],
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: 'Plaintiffs,',
                                alignment: 'right',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: 'VERIFICATION',
                                bold: true,
                                fontSize: 14,
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: '-against-',
                                alignment: 'center',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: 'Index No.: ',
                                border: [false, true, false, false]
                            }
                        ],
                        [
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: `${case_number}`,
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: `${case_defendant_name || ''}`,
                                bold: true,
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                        [
                            {
                                text: 'Defendants.',
                                alignment: 'right',
                                border: [false, false, false, true]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: '',
                                border: [false, false, false, false]
                            }
                        ],
                    ],
                },
            },
            {
                styles: 'footerTable',
                table: {
                    widths: [200, 100, '*'],
                    heights: 12,
                    body: [
                        [
                            {
                                text: 'STATE OF NEW YORK',
                                border: [false, false, false, false]
                            },
                            {
                                text: ')',
                                border: [false, false, false, false]
                            },
                        ],
                        [
                            {
                                text: '',
                                border: [false, false, false, false]
                            },
                            {
                                text: ').ss.:',
                                border: [false, false, false, false]
                            },
                        ],
                        [
                            {
                                text: `COUNTY OF ${county || ''}`,
                                border: [false, false, false, false]
                            },
                            {
                                text: ')',
                                border: [false, false, false, false]
                            },
                        ]
                    ]
                },
            },
            {
                text: '\n\n\n'
            },
            {
                text: [
                    { text: `\u200B\t\t\t\u200B\t\t\t` },
                    `I, `, { text: `${client_name || ''}, `, bold: true, fontSize: 15 },
                    `state that I have read the foregoing `,
                    { text: `${document_type && getType(document_type, state) || ''}`, bold: true },
                    ` and know the contents thereof; the same is true to my knowledge, except as to the matters therein stated to be alleged on information and belief, and as to those matters I believe it to be true.  The grounds of my belief as to all matters not stated upon my own knowledge are as follows: information and investigation maintained in this office.`

                ],
                style: 'body'
            },
            {
                text: '\n\n\n',
            },
            signatureData ? [{
                image: signatureData,
                fit: [120, 120],
                alignment: 'right',
                decoration: 'underline',
                lineHeight: 1.8,
                margin: [0, 10, 0, 0]
            },
            {
                text: `________________________`,
                alignment: 'right',
                bold: true
            }] :
                {
                    text: `________________________`,
                    alignment: 'right',
                    bold: true
                },
            {
                text: `${client_name || ''}`,
                bold: true,
                fontSize: 14,
                alignment: 'right',
                margin: [0, 10, 0, 0],
            }
        ],
        styles: {
            tableStyle: {
                margin: [0, 5, 0, 15]
            },
            body: {
                lineHeight: 1.5,
                fontSize: 14,
                margin: [10, 15, 0, 0]
            },
            footerTable: {
                margin: [0, -10, 0, 0],
            }
        }
    } : {
        content: questionsDef.concat([
            {
                text: TargetLanguageCode === 'es' ? 'VERIFICACIÓN' : TargetLanguageCode === 'vi' ? 'XÁC MINH' : 'VERIFICATION',
                style: 'title',
                alignment: 'center'
            },
            {
                text: [
                    { text: `&nbsp;`, color: '#FFFFFF' },
                    TargetLanguageCode === 'es' ? `Soy una de las partes en la demanda denominada ` : TargetLanguageCode === 'vi' ? `Tôi là một bên trong hành động có quyền ` : `I am a party in the action entitled `,
                    { text: `${case_title || ''}`, italics: true },
                    TargetLanguageCode === 'es' ? `. Tengo conocimiento del contenido del documento ` : TargetLanguageCode === 'vi' ? `. Tôi đã quen với nội dung của những điều đã nói ở trên ` : `. I am familiar with the contents of the foregoing `,
                    { text: `${document_type && getDefendantType(document_type, state, set_number) || ''}`, bold: true },
                    TargetLanguageCode === 'es' ? ' antes mencionado. La información proporcionada en el presente documento está basada en mi conocimiento personal y/o ha sido aportada por mis abogados u otros agentes y, por lo tanto, es proporcionada conforme a la ley. La información contenida en el documento antes mencionado es verdadera, excepto en los asuntos que fueron aportados por mis abogados u otros agentes, y, en cuanto a esos asuntos, estoy enterado y considero que son verdaderos. \n' : TargetLanguageCode === 'vi' ? '. Thông tin được cung cấp trong đó dựa trên kiến ​​thức cá nhân của tôi và / hoặc đã được cung cấp bởi luật sư của tôi hoặc các đại lý khác và do đó được cung cấp theo yêu cầu của pháp luật. Thông tin trong tài liệu nói trên là đúng sự thật, ngoại trừ những vấn đề được cung cấp bởi luật sư của tôi hoặc những người đại diện khác, và về những vấn đề đó, tôi được thông báo và tin rằng chúng là sự thật. \n' : '. The information supplied therein is based on my own personal knowledge and/ or has been supplied by my attorneys or other agents and is therefore provided as required by law. The information contained in the foregoing document is true, except as to the matters which were provided by my attorneys or other agents, and, as to those matters, I am informed and believe that they are true. \n',
                    { text: `&nbsp;`, color: '#FFFFFF' },
                    TargetLanguageCode === 'es' ? `Declaro bajo pena de perjurio en conformidad con las leyes del estado de California que las respuestas anteriores son verdaderas y correctas. \n` : TargetLanguageCode === 'vi' ? `Tôi tuyên bố sẽ chịu hình phạt về tội khai man theo luật của bang California rằng các câu trả lời ở trên là đúng và chính xác. \n` :
                        {
                            text: [
                                'I declare under penalty of perjury under the laws of',
                                `${federal ? '' : ' the state of'}`,
                                {
                                    text: [
                                        federal ? '' : ['PL', 'NPL'].includes(state) ? {
                                            text: [
                                                ' ',
                                                { text: 'STATE NAME', background: 'yellow' }
                                            ]
                                        } : { text: state === 'TN' ? ` Tennessee` : state === 'TX' ? ` Texas` : state === 'FL' ? ' Florida' : state === 'NV' ? ' Nevada' : state === 'GA' ? ` Georgia` : state === 'AZ' ? ` Arizona` : state === 'WA' ? ` Washington` : state === 'MI' ? ` Michigan` : ` California` }
                                    ]
                                },
                                ` that the foregoing responses are true and correct. \n`
                            ]
                        },
                    { text: `&nbsp;`, color: '#FFFFFF' },
                    TargetLanguageCode === 'es' ? `Firmado el ` : TargetLanguageCode === 'vi' ? `Đã thực thi vào ` : `Executed on `,
                    { text: `${moment().format('MM/DD/YYYY')}`, decoration: 'underline' },
                    TargetLanguageCode === 'es' ? `, en ` : TargetLanguageCode === 'vi' ? `, tại ` : `, at `,
                    { text: `${county || ''}`, decoration: 'underline' },
                    federal ? '.\n' : {
                        text: [
                            ['PL', 'NPL'].includes(state) ? {
                                text: [
                                    ', ',
                                    { text: 'STATE NAME', background: 'yellow' },
                                    '. \n'
                                ]
                            } : { text: state === 'TN' ? ', Tennessee. \n' : state === 'TX' ? ', Texas. \n' : state === 'FL' ? ', Florida. \n' : state === 'NV' ? ', Nevada. \n' : state === 'GA' ? ', Georgia. \n' : state === 'AZ' ? ', Arizona. \n' : state === 'WA' ? ', Washington. \n' : state === 'MI' ? ', Michigan. \n' : ', California. \n' },
                        ]
                    },
                ],
                style: 'body'
            },
            signatureData ? [{
                image: signatureData,
                fit: [120, 120],
                alignment: 'right',
                decoration: 'underline',
                lineHeight: 1.8,
                margin: [0, 10, 0, 0]
            },
            {
                text: `________________________`,
                alignment: 'right',
                bold: true
            }] : {
                text: TargetLanguageCode === 'es' ? `La firma va aquí` : TargetLanguageCode === 'vi' ? `Chữ ký ở đây` : `Signature goes here`,
                bold: true,
                color: '#3867BB',
                fontSize: 18,
                alignment: 'right',
                decoration: 'underline',
                lineHeight: 1.8,
                margin: [0, 25, 0, 0]
            },
            {
                text: `${client_name || ''}`,
                bold: true,
                fontSize: 16,
                alignment: 'right',
                margin: [0, 10, 0, 0],
            }
        ]),
        styles: {
            title: {
                fontSize: 18,
                bold: true,
                alignment: 'justify',
                decoration: 'underline',
                lineHeight: 1.3,
                margin: [0, 45, 0, 0]
            },
            body: {
                lineHeight: 1.4,
                fontSize: 15,
                margin: [10, 15, 0, 0]
            },
            questions: {
                bold: true,
                fontSize: 14,
                margin: [0, 15, 0, 4]
            },
            response: {
                fontSize: 13,
                lineHeight: 1.2
            }
        }
    }
}


const getDefendantType = (value, state, set_number) => {
    const setNumber = set_number && set_number.toLowerCase() === 'two' ? 'SET TWO' : 'SET ONE';
    switch (value.toUpperCase()) {
        case 'FROGS': return `RESPONSES TO FORM INTERROGATORIES, ${setNumber}`;
        case 'SPROGS': return state !== 'CA' ? `RESPONSES TO INTERROGATORIES, ${setNumber}` : `RESPONSES TO SPECIALLY PREPARED INTERROGATORIES, ${setNumber}`;
        case 'RFPD': return `RESPONSES TO REQUESTS FOR PRODUCTION OF DOCUMENTS, ${setNumber}`;
        case 'RFA': return `RESPONSES TO REQUESTS FOR ADMISSIONS, ${setNumber}`;
    }
}

const getDocumentType = (type) => {
    switch (type.toUpperCase()) {
        case 'SPROGS': return `Answers to Interrogatories`;
        case 'RFPD': return `Answers to Requests for Production of Documents`;
        case 'RFA': return `Answers to Requests for Admissions`;
    }
}

const getType = (value, state) => {
    switch (value.toUpperCase()) {
        case 'BOP': return `BILL OF PARTICULARS`;
        case 'ODD': return `OMNIBUS DISCOVERY DEMANDS`;
        case 'SPROGS': return `RESPONSES TO INTERROGATORIES, SET ONE`;
        case 'NDI': return `NOTICE FOR DISCOVERY AND INSPECTION`;
        case 'NTA': return `NOTICE TO ADMIT`;
    }
}