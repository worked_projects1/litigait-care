/**
 * 
 * Document
 * 
 */


import React, { useState } from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import SignatureForm from '../../components/SignatureForm';
import PDFMaker from '../../components/PDFMaker';
import styles from './styles';
import withQueryParams from 'react-router-query-params';
import { compose } from 'redux';

function Document(props) {
    const { handleDocumentSubmit, handleBackPage, records = [], docDefinition, queryParams = {}, style, onError } = props;
    const { TargetLanguageCode } = queryParams;
    const [formData, setFormData] = useState({});
    const { County: county } = formData;
    const record = records && records.length > 0 && records[0];
    const { case_title, document_type, client_name, state, federal, case_plaintiff_name, case_defendant_name, case_number, set_number } = record;
    const classes = styles();

    const handlePreview = (openPDF) => {
        if (county) {
            openPDF();
        } else {
            const errorMsg = TargetLanguageCode == 'es' ? `Por favor ingrese el condado.` : TargetLanguageCode == 'vi' ? `Vui lòng nhập quận.` : `Please enter the county.`;
            onError(errorMsg);
        }
    }

    const handlePDF = (data) => {
        const newWindow = window.open("/view-pdf", "_blank");
        newWindow.wbase_64 = data;
    }

    return <Grid style={style} className={classes.document}>
        <Grid item xs={12} >
            <Typography variant="h3" className={classes.electronicSignature}>
                <b>{TargetLanguageCode === 'es' ? 'Firma electrónica del documento' : TargetLanguageCode === 'vi' ? `Chứng từ Chữ ký Điện tử ` : 'Document Electronic Signature'}</b><br />
            </Typography>
        </Grid>
        <Grid item xs={12}>
            <Grid container direction="row" justify="space-between">
                <Grid>
                    <Typography style={{ marginTop: "12px" }} >
                        <b>{TargetLanguageCode === 'es' ? 'Revise el documento antes de firmar' : TargetLanguageCode === 'vi' ? 'Xem lại tài liệu trước khi ký' : 'Review document before signing'}</b>
                    </Typography>
                </Grid>
                <Grid>
                    <PDFMaker
                        type='_base64'
                        callback={handlePDF}
                        docDefinition={docDefinition({ questions: records, case_title, county, document_type, client_name, TargetLanguageCode, state, federal, case_plaintiff_name, case_defendant_name, case_number, set_number })}>
                        {(preview) =>
                            <Button className={classes.btnVerify} type="button" variant="contained" color="primary" onClick={() => handlePreview(preview)}>
                                {TargetLanguageCode === 'es' ? 'Avance' : TargetLanguageCode === 'vi' ? 'Xem trước' : 'Preview'}
                            </Button>}
                    </PDFMaker>
                </Grid>
            </Grid>
        </Grid>
        <Grid item xs={12} style={{ marginTop: '25px' }}>
            <SignatureForm
                onChangeData={(e) => setFormData(e)}
                handleSubmit={handleDocumentSubmit}
                handleBackPage={handleBackPage}
                TargetLanguageCode={TargetLanguageCode}
            />
        </Grid>
    </Grid>
}

export default compose(
    withQueryParams()
)(Document);