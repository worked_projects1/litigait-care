import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    document: {
        padding: '20px'
    },
    electronicSignature: {
        marginBottom: theme.spacing(8),
        marginTop: theme.spacing(2),
        fontSize: '22px',
        color: '#001b69',
        textAlign: "center"
    },

    btnVerify: {
        fontSize: '18px',
        width: '128px',
        fontWeight: 'bold',
        padding: '6px 0px',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        '&:hover': {
            background: '#2ca01c',
        }
    }

}));


export default useStyles;