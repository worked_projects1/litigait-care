/**
 * 
 * Questionnaire
 * 
 */


import React, { useEffect, useState } from 'react';
import { Grid, Typography, Link } from '@material-ui/core';
import styles from './styles';
import QuestionnaireForm from '../../components/QuestionnaireForm';
import api from '../../Utils/api';
import CheckBoxRoundedIcon from '@material-ui/icons/CheckBoxRounded';
import Verification from '../Verification/Verification';
import styled from 'styled-components';
import Introduction from '../Introduction/Introduction';
import ClipLoader from 'react-spinners/ClipLoader';
import store2 from 'store2';
import Snackbar from '../../components/Snackbar';
import withQueryParams from 'react-router-query-params';
import { compose } from 'redux';
import lodash from 'lodash';
import { skipOptions, defaultQuestion, consultationNote } from './utils';
import { connect } from 'react-redux';
import { destroy } from 'redux-form';

const ProgressBarWrapper = styled.div`
  position: fixed;
  max-width: 620px;
  margin: auto;
  left: 0;
  right: 0;
  top: 0;
  background-color: white;
  box-shadow: 0 0 8px rgba(0, 12, 63, 0.15);
  padding: 1em;
  z-index: 10;
  display: flex;

  @media only screen and (max-width: 1200px) {
    max-width: initial;
  }
`;

const ProgressBarInner = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
  height: 1em;
  border-radius: 2em;
  overflow: hidden;
`;

const ProgressBarBase = styled.div`
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  background: rgba(0, 12, 63, .1);
`;

const ProgressBar = styled.div`
  position: absolute;
  left: 0;
  width: ${props => Math.ceil(props.width)}%;
  height: 100%;
  background: #1BD588;
  transition: width .3s ease-in-out;
`;

const ProgressText = styled.div`
  font-size: .875em;
  font-weight: 500;
  opacity: .5;
  white-space: nowrap;
  margin-left: 1em;
`;


const ClipWrapper = styled.div`
  text-align: center;
  margin-top: 100px
`;


function Questionnaire(props) {
    const { history, queryParams = {}, dispatch } = props;
    const { TargetLanguageCode } = queryParams;
    const classes = styles();
    const [record, setRecord] = useState(false);
    const [questionsData, setQuestionsData] = useState(false);
    const [error, setError] = useState(false);
    const [success, setSuccess] = useState(false);
    const [activeStep, setActiveStep] = useState(0);
    const [showTitle, setShowTitle] = useState(true);
    const [showQuestionnaire, setShowQuestionnaire] = useState(false);
    const [answeredCount, setAnsweredCount] = useState(0);
    const [busy, setBusy] = useState(false);
    const [showVerification, setShowVerification] = useState(false);
    const [resendCount, setResendCount] = useState(0);
    const [lawFirm, setLawFirm] = useState(false);
    const questionCategory = questionsData && questionsData?.length > 0 && questionsData[0] && questionsData[0]?.question_category && questionsData[0]?.document_type && questionsData[0]?.document_type.toLowerCase() === 'odd' || false;
    const questions = questionCategory ? lodash.groupBy(questionsData, 'question_category_id') : lodash.groupBy(questionsData, 'question_number_text') || {};

    const FetchRecord = async () => {
        setBusy(true);
        await api.get(`forms/get-questions-by-otp?otp_code=${props.match.params.token}&TargetLanguageCode=${TargetLanguageCode}`).then((response) => {
            const questionCategory = response && response.data && response.data?.length > 0 && response.data?.[0] && response.data?.[0]?.question_category && response.data?.[0]?.document_type && response.data?.[0]?.document_type.toLowerCase() === 'odd' || false;
            const quesObj = questionCategory ? response && response.data && lodash.groupBy(response.data, 'question_category_id') : response && response.data && lodash.groupBy(response.data, 'question_number_text') || {};
            setRecord(quesObj && Object.keys(quesObj).length > 0 && Object.keys(quesObj).map(r => r) || []);
            setQuestionsData(response.data);
            const ansQues = response.data && response.data.length > 0 ? response.data.filter(q => q.client_response_text) : [];
            setAnsweredCount(ansQues.length);
            setBusy(false);
        }).catch((error) => {
            history.push(`/`);
            setBusy(false);
            setError(error);
        }).finally(() => {
            // setBusy(false);
        });
    }

    useEffect(() => {

        const otp = store2.get('otp');
        const token = store2.get('token');
        const previousTimestamp = store2.get('previous_timestamp')
        const clientAnswered = store2.get('client_answered')
        const currentTimestamp = Math.floor(Date.now() / 1000);

        if (otp && token === props.match.params.token) {
            if (currentTimestamp && previousTimestamp && clientAnswered && (currentTimestamp - previousTimestamp > 3600))
                sendOtp();
            else
                handleVerification(otp);
        } else {
            if (previousTimestamp && clientAnswered && token !== props.match.params.token) {
                store2.remove('previous_timestamp');
                store2.remove('client_answered');
            }
            setShowVerification(true);
        }
    }, []);

    const sendOtp = async () => {
        setResendCount(resendCount => resendCount + 1);
        setBusy(true);
        await api.post(`clients/sent-otp`, Object.assign({}, { otp_code: props.match.params.token, TargetLanguageCode })).then((response) => {
            setShowVerification(true);
        }).catch((error) => {
            history.push(`/`);
            setError(error);
        }).finally(() => {
            setBusy(false);
        });
    }

    const handleContinue = () => {
        setShowQuestionnaire(true);
    }

    const verificationApi = async (securityCode) => {
        setBusy(true);
        await api.post(`clients/validate-otp`, Object.assign({}, { otp_code: props.match.params.token, otp_secret: securityCode })).then((response) => {
            store2.set('otp', securityCode);
            store2.set('token', props.match.params.token);
            setShowVerification(false);
            setLawFirm(response.data);
            FetchRecord();
        }).catch((error) => {
            //history.push(`/`);
            const Err = error && error.response && error.response.data && error.response.data.error && typeof error.response.data.error === 'string' ? error.response.data.error === `Invalid OTP` ? TargetLanguageCode === 'es' ? 'El código ingresado no es válido. Inténtalo de nuevo.' : TargetLanguageCode === 'vi' ? `Mã đã nhập không hợp lệ. Vui lòng thử lại.` : `Entered code is not valid. Please try again.` : error.response.data.error : "Entered code is not valid. Please try again.";
            setBusy(false);
            setError(Err);
        }).finally(() => {
            // setBusy(false);
        });
    }

    const handleVerification = (securityCode) => {
        verificationApi(securityCode);
    }

    const handleVerify = (securityCode) => {
        verificationApi(securityCode);
        const currentTimestamp = Math.floor(Date.now() / 1000);
        store2.set('previous_timestamp', currentTimestamp);
    }

    const handleSubmit = async (data, dispatch, { form }) => {
        let newData;
        let formData = [];
        let finalFormData;
        const questionRecord = questions && record[activeStep] && questions[record[activeStep]] || [];

        if (data && data['questions'] && data['questions'][0] && data['questions'][0][0] && data['questions'][0][0].is_consultation_set && data['questions'][0][0].is_consultation_set == 1) {
            data['questions'].map((arr, index) => {
                let split = Object.keys(arr).map(i => arr[i]);
                formData.push(...split);
            });
            const filterFormData = formData.filter(_ => _.client_response_text !== null);
            finalFormData = filterFormData;
        } else {
            const filterFormData = data['questions'].reduce((acc, elm) => {
                if (!elm.client_response_text && (elm.document_type == 'rfpd' || elm.document_type == 'RFPD') && elm.uploaded_documents && elm.uploaded_documents.length > 0) {
                    acc.push(Object.assign({}, elm, { client_response_text: 'See attached files' }));
                } else if (elm.client_response_text !== null) {
                    acc.push(elm);
                }
                return acc;
            }, []);
            finalFormData = filterFormData;
        }

        if (finalFormData && finalFormData.length > 0) {
            let result = finalFormData.filter(el => questionRecord.some(val => {
                if (el.id == val.id) {
                    let record_uploaded_documents = val.uploaded_documents && typeof val.uploaded_documents === 'string' && JSON.parse(val.uploaded_documents) || [];
                    if (el.uploaded_documents && el.document_type.toLowerCase() == 'rfpd') {
                        return (el.client_response_text != val.client_response_text || el.uploaded_documents.length != record_uploaded_documents.length)
                    } else {
                        return el.client_response_text != val.client_response_text;
                    }
                }
            }));

            let finalRecord = result && Array.isArray(result) && result.length > 0 && result.reduce((acc, el) => {
                if (el && (el['client_response_status'] && el['client_response_status'].toString() === 'ClientResponseAvailable' && el['lawyer_response_text'])) {
                    acc.push(Object.assign({}, el, { is_the_client_response_edited: true }));
                } else {
                    acc.push(Object.assign({}, el, { is_the_client_response_edited: false }));
                }
                return acc;
            }, []);

            newData = Object.assign({}, { questions: finalRecord || [] });
        }

        const submitRecord = newData && newData.questions && newData.questions.length > 0 ? newData.questions.map(q => q.client_response_text ? Object.assign({}, q, { client_response_status: "ClientResponseAvailable", uploaded_documents: q.uploaded_documents && JSON.stringify(q.uploaded_documents) }) : Object.assign({}, q, { uploaded_documents: q.uploaded_documents && JSON.stringify(q.uploaded_documents) })) : [];

        if (submitRecord && submitRecord.length > 0) {
            setBusy(true);
            await api.post(`forms/save-client-response?otp_code=${props.match.params.token}`, submitRecord).then((response) => {
                setSuccess(response.message);
                store2.set('client_answered', true);
            }).catch((error) => {
                setError(error);
            }).finally(() => {
                const questionIds = (submitRecord || []).map(c => c.id);

                const updatedQuestions = questionsData && questionsData.length > 0 && questionsData.map(c => questionIds.includes(c.id) ? Object.assign({}, c, { ...submitRecord.find(_ => _.id === c.id) }) : c);

                setQuestionsData(updatedQuestions);

                const ansQues = updatedQuestions && updatedQuestions.length > 0 ? updatedQuestions.filter(q => q.client_response_text) : [];

                if (record && activeStep === record.length - 1)
                    setShowTitle(false);

                setActiveStep((prevActiveStep) => prevActiveStep + 1);
                setAnsweredCount(ansQues.length);
                setBusy(false);
            });

        } else {
            if (record && activeStep === record.length - 1)
                setShowTitle(false);

            setActiveStep((prevActiveStep) => prevActiveStep + 1);
        }
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        })

    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    };

    const handleSkip = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    }

    const handleSkipQuestion = async (data, skipValue, initialize) => {
        if (data && data.length > 0 && skipValue) {
            let submitRecord = data.map(q => Object.assign({}, q, { client_response_text: skipValue, client_response_status: "ClientResponseAvailable" }));
            let setInitialFormValue = data.map(q => Object.assign({}, q, { client_response_text: skipValue }));
            if (data && data[0] && data[0].is_consultation_set && data[0].is_consultation_set == 1) {
                const groupedQues = setInitialFormValue && Array.isArray(setInitialFormValue) && setInitialFormValue.length > 0 && setInitialFormValue[0].is_consultation_set && lodash.groupBy(setInitialFormValue, 'consultation_set_no');
                const groupedQuesArray = groupedQues && Object.keys(groupedQues).map((el, i) => Object.assign({}, groupedQues[el]));
                // Set initialValue
                let initialValue = Object.assign({}, { questions: groupedQuesArray });
                initialize(initialValue);

            } else {
                // Set initialValue
                let initialValue = Object.assign({}, { questions: setInitialFormValue });
                initialize(initialValue);
            }

            if (submitRecord && submitRecord.length > 0) {
                setBusy(true);
                await api.post(`forms/save-client-response?otp_code=${props.match.params.token}`, submitRecord).then((response) => {
                    setSuccess(response.message);
                    store2.set('client_answered', true);
                }).catch((error) => {
                    setError(error);
                }).finally(() => {
                    const questionIds = (submitRecord || []).map(c => c.id);

                    const updatedQuestions = questionsData && questionsData.length > 0 && questionsData.map(c => questionIds.includes(c.id) ? Object.assign({}, c, { ...submitRecord.find(_ => _.id === c.id) }) : c);

                    setQuestionsData(updatedQuestions);

                    const ansQues = updatedQuestions && updatedQuestions.length > 0 ? updatedQuestions.filter(q => q.client_response_text) : [];

                    if (record && activeStep === record.length - 1)
                        setShowTitle(false);

                    setActiveStep((prevActiveStep) => prevActiveStep + 1);
                    setAnsweredCount(ansQues.length);
                    setBusy(false);
                });

            } else {
                if (record && activeStep === record.length - 1)
                    setShowTitle(false);

                setActiveStep((prevActiveStep) => prevActiveStep + 1);
            }
        }
    }

    const handleAddConsultation = async (subQuestionSet, setLoader) => {
        const documentType = subQuestionSet && subQuestionSet[0] && subQuestionSet[0][0].document_type || false;
        const addConsultation = documentType === 'IDC' ? defaultQuestion(subQuestionSet[subQuestionSet.length - 1]) : defaultQuestion(subQuestionSet[0]);
        const requestData = addConsultation.map(r => Object.assign({}, r, { otpCode: props.match.params.token }));

        await api.post(`rest/forms/add-new-client-consultation`, requestData).then((response) => {
            let res = response.data;
            let result = res && Array.isArray(res) && res.length > 0 && questionsData && Array.isArray(questionsData) && questionsData.length > 0 && questionsData.map(el => {
                if (el && el.is_consultation_set) {
                    return Object.assign({}, el, { total_consultation_count: res[0].total_consultation_count })
                } else {
                    return el;
                }
            });
            setQuestionsData([...result, ...res]);
            setLoader(false);
        }).catch((error) => {
            history.push(`/`);
            setError(error);
        }).finally(() => {
            setLoader(false);
        });
    }

    const deleteConsultation = async (data, initialize) => {
        let submitData = data && data[0] || [];
        const submitRecord = Object.assign({}, {
            id: submitData.case_id,
            case_id: submitData.case_id,
            practice_id: submitData.practice_id,
            client_id: submitData.client_id,
            party_id: submitData.party_id || false,
            legalforms_id: submitData.legalforms_id,
            document_type: submitData.document_type,
            question_ids: data && data.map(el => el.id),
            otp_code: props.match.params.token
        });

        await api.post(`rest/forms/remove-consultation-client`, submitRecord).then((response) => {
            let question_ids = data && data.map(el => el.id);
            let res = response && response.data;
            let consultationRecord = questionsData && questionsData.filter(e => !question_ids.includes(e.id));

            consultationRecord = consultationRecord && consultationRecord.length > 0 && consultationRecord.map(el => Object.assign({}, { ...el }, { total_consultation_count: res[0].total_consultation_count }));
            let consultation_no = submitData && submitData.consultation_set_no;

            let finalConsultationRecord = (consultationRecord || []).reduce((a, el) => {
                if (el && el.is_consultation_set) {
                    if (el.consultation_set_no > consultation_no && el.consultation_set_no !== consultation_no) {
                        a.push(Object.assign({}, { ...el }, { consultation_set_no: (consultation_no - 1) === el.consultation_set_no ? el.consultation_set_no : el.consultation_set_no - 1 }));
                    } else if (el && el.consultation_set_no < consultation_no) {
                        a.push(el);
                    }
                } else {
                    a.push(el);
                }
                return a;
            }, []);
            const documentType = finalConsultationRecord && finalConsultationRecord.length && finalConsultationRecord[0].document_type || false;
            const consultationMessage = TargetLanguageCode == 'es' ? `Consulta eliminada con éxito` : TargetLanguageCode == 'vi' ? `Consultaion đã được xóa thành công` : documentType === 'IDC' ? `Question removed successfully` : `Consultaion removed successfully`;
            setSuccess(consultationMessage);
            setQuestionsData(finalConsultationRecord);

            const questionsRecord = finalConsultationRecord && lodash.groupBy(finalConsultationRecord, 'question_number_text') || {};
            let questionRecords = questionsRecord && record[activeStep] && questionsRecord[record[activeStep]] || [];
            const groupedQues = questionRecords && Array.isArray(questionRecords) && questionRecords.length > 0 && questionRecords[0].is_consultation_set && lodash.groupBy(questionRecords, 'consultation_set_no');
            const groupedQuesArray = groupedQues && Object.keys(groupedQues).map((el, i) => Object.assign({}, groupedQues[el]));
            // Set initialValue
            let initialValue = Object.assign({}, { questions: groupedQuesArray });
            initialize(initialValue);
        }).catch((error) => {
            history.push(`/`);
            setError(error);
        }).finally(() => {
            setBusy(false);
        });
    }

    const FetchEditedRecord = async () => {
        setBusy(true);
        await api.get(`/rest/forms/get-all-questions-by-otp?otp_code=${props.match.params.token}&TargetLanguageCode=${TargetLanguageCode}`).then((response) => {

            const questionCategory = response && response.data && response.data?.length > 0 && response.data?.[0] && response.data?.[0]?.question_category && response.data?.[0]?.document_type && response.data?.[0]?.document_type.toLowerCase() === 'odd' || false;
            const quesObj = questionCategory ? response && response.data && lodash.groupBy(response.data, 'question_category_id') : response && response.data && lodash.groupBy(response.data, 'question_number_text') || {};
            setRecord(quesObj && Object.keys(quesObj).length > 0 && Object.keys(quesObj).map(r => r) || []);
            setQuestionsData(response.data);
            const ansQues = response.data && response.data.length > 0 ? response.data.filter(q => q.client_response_text) : [];
            setAnsweredCount(ansQues.length);
            setBusy(false);
        }).catch((error) => {
            history.push(`/`);
            setBusy(false);
            setError(error);
        }).finally(() => {
            // setBusy(false);
        });
    }

    const handleEditAnswer = () => {
        FetchEditedRecord();
        setShowQuestionnaire(true);
        setActiveStep(0);
        setShowTitle(true);
        questions && Object.keys(questions).map((e, i) => {
            dispatch(destroy(`questionaireForm_${i}`))
        });
    }

    if (busy && !showQuestionnaire) {
        return <ClipWrapper>
            <ClipLoader color={"#2ca01c"} />
        </ClipWrapper>
    }

    return (
        <Grid container>
            {record && record.length > 0 && !showQuestionnaire ? <Introduction handleContinue={handleContinue} totalQuestions={record.length}
                title={TargetLanguageCode === 'es' ? "Cuestionario para tu caso" : TargetLanguageCode === 'vi' ? "Bảng câu hỏi cho trường hợp của bạn" : "Questionnaire for your case"}
                content={TargetLanguageCode === 'es' ? `Sus abogados en <b>${lawFirm.practice_name}</b> están solicitando sus respuestas y le enviado ${questionsData.length} preguntas para prepararse para la demanda. Responda lo antes posible.` : TargetLanguageCode === 'vi' ? `Các Luật sư của bạn tại <b>${lawFirm.practice_name}</b> đang yêu cầu phản hồi của bạn và đã gửi cho bạn ${questionsData.length} câu hỏi để chuẩn bị cho vụ kiện. Vui lòng trả lời sớm nhất có thể.` : `Your Lawyers at <b>${lawFirm.practice_name}</b> are requesting your responses and sent you ${questionsData.length} questions to prepare for the lawsuit. Please answer at your earliest convenience.`}
                QuestionInstructions={TargetLanguageCode === 'es' ? 'Es importante que responda todas las preguntas con la mayor cantidad de información relevante posible, ya que esta es su oportunidad de explicar y presentar su caso. Además, tenga en cuenta que está bajo juramento de que sus respuestas son verdaderas.<br /><br /> Una vez que responda todas las preguntas, su abogado revisará sus respuestas y preparará su defensa.' : TargetLanguageCode === 'vi' ? `Điều quan trọng là bạn phải trả lời tất cả các câu hỏi với càng nhiều thông tin liên quan càng tốt vì đây là cơ hội để bạn giải thích và đưa ra trường hợp của mình. Ngoài ra, xin lưu ý rằng bạn tuyên thệ rằng câu trả lời của bạn là đúng.<br /><br /> Sau khi bạn trả lời tất cả các câu hỏi, luật sư của bạn sẽ xem xét câu trả lời của bạn và chuẩn bị bào chữa cho bạn.` : 'It is important that you answer all questions with as much relevant information as you can since this is your chance to explain and make your case. Also, please note that you are under oath that your answers are true.<br /><br /> Once you answer all questions, your attorney will review your responses and prepare your defense.'} /> :
                record ?
                    <Grid container item spacing={3} justify="center" style={{ margin: '20px', width: '100%', marginRight: '30px' }}>
                        <Grid style={{ maxWidth: '700px' }}>
                            {record && activeStep !== record.length ?
                                <ProgressBarWrapper style={{ maxWidth: '680px' }}>
                                    <ProgressBarInner>
                                        <ProgressBarBase />
                                        <ProgressBar width={(parseInt(answeredCount) / questionsData.length) * 100} />
                                    </ProgressBarInner>
                                    <ProgressText>
                                        {Math.ceil((parseInt(answeredCount) / questionsData.length) * 100)}%
                                    </ProgressText>
                                </ProgressBarWrapper> : null}
                            {busy ? <ClipWrapper>
                                <ClipLoader color={"#2ca01c"} />
                            </ClipWrapper> :
                                <Grid item xs={12}>
                                    {record && activeStep === record.length ? (
                                        <Grid container justify="center" spacing={2}>
                                            <Grid item xs={12}>
                                                <CheckBoxRoundedIcon className={classes.checkIcon} style={{ width: '100%', height: '100px' }} />
                                            </Grid>
                                            <Typography variant="subtitle2" className={classes.thankyou}><b>{TargetLanguageCode === 'es' ? 'Gracias' : TargetLanguageCode === 'vi' ? 'Cảm ơn' : 'Thank You'}</b></Typography>
                                            <Typography className={classes.instructions}>{TargetLanguageCode === 'es' ? 'El cuestionario se presentó con éxito. El abogado tal vez le llame para pedirle más aclaraciones.' : TargetLanguageCode === 'vi' ? 'Bảng câu hỏi đã được gửi thành công. Luật sư có thể gọi cho bạn để làm rõ thêm.' : 'The questionnaire was submitted successfully. The Lawyer may call you for further clarification.'}</Typography>
                                            <Link className={classes.btnContinue}
                                                component="button"
                                                variant="body2"
                                                color="primary"
                                                underline="always"
                                                onClick={handleEditAnswer}
                                            >
                                                {TargetLanguageCode === 'es' ? 'Haga clic aquí si necesita modificar su respuesta' : TargetLanguageCode === 'vi' ? 'Nhấp vào đây nếu bạn cần sửa đổi câu trả lời của mình' : 'Click here if you need to modify your response'}
                                            </Link>
                                        </Grid>) : (
                                        <Grid style={showTitle ? { marginTop: '80px' } : null}>
                                            <Grid>
                                                {showTitle ?
                                                    <Grid item xs={12}>
                                                        <Grid container direction="row" justify="space-between">
                                                            <Grid>
                                                                <Typography variant="subtitle2" className={classes.questionnaire}>
                                                                    <b>{TargetLanguageCode === 'es' ? 'Cuestionario' : TargetLanguageCode === 'vi' ? 'bảng câu hỏi' : 'Questionnaire'}</b>
                                                                </Typography>
                                                            </Grid>
                                                        </Grid>
                                                    </Grid> : null}

                                            </Grid>
                                            <Grid>
                                                {record && record.length > 0 ? <QuestionnaireForm
                                                    key={activeStep}
                                                    activeStep={activeStep}
                                                    questionNumber={activeStep + 1}
                                                    steps={record}
                                                    records={questions && record[activeStep] && questions[record[activeStep]] || []}
                                                    onSubmit={handleSubmit}
                                                    handleBack={handleBack}
                                                    form={`questionaireForm_${activeStep}`}
                                                    success={success}
                                                    handleSkip={handleSkip}
                                                    handleSkipQuestion={handleSkipQuestion}
                                                    skipOptions={skipOptions(TargetLanguageCode)}
                                                    handleAddConsultation={handleAddConsultation}
                                                    deleteConsultation={deleteConsultation}
                                                    consultationNote={consultationNote(TargetLanguageCode)}
                                                /> : null}
                                            </Grid>
                                        </Grid>
                                    )}
                                </Grid>
                            }
                        </Grid>
                    </Grid>
                    // : null}
                    : showVerification ? <Verification handleVerify={handleVerify} error={error} sendOtp={sendOtp} resendCount={resendCount} /> : null}
            <Snackbar show={error ? true : false} text={error} severity={'error'} handleClose={() => setError(false)} />
            <Snackbar show={success ? true : false} text={success} severity={'success'} handleClose={() => setSuccess(false)} />
        </Grid>
    );
}

const mapDispatchToProps = (dispatch, props) => {
    return {
        dispatch
    }
}

export default compose(
    connect(mapDispatchToProps),
    withQueryParams()
)(Questionnaire);
