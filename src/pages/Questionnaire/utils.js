/**
 * 
 * Utils
 * 
 */


 export const skipOptions = (code) => [
    `${code === 'es' ? 'no entendí la pregunta' : code === 'vi' ? 'Tôi không hiểu câu hỏi' : 'I did not understand the question'}.`,
    `${code === 'es' ? 'Todavía estoy obteniendo la información relevante para responder esta pregunta' : code === 'vi' ? 'Tôi vẫn đang lấy thông tin liên quan để trả lời câu hỏi này' : 'I am still obtaining the relevant information to answer this question'}.`,
    `${code === 'es' ? 'Quiero responder esta pregunta después de terminar las otras preguntas' : code === 'vi' ? 'Tôi muốn trả lời câu hỏi này sau khi kết thúc các câu hỏi khác' : 'I want to answer this question after finishing the other questions'}.`
];


/**
 * Frogs disc001 6.4 questions
 */
 const defaultQuestionsData = (record) => {
    let question_text = "Did you receive any consultation or examination (except from expert witnesses covered by Code of Civil Procedure sections 2034.210-2034.310) or treatment from a HEALTH CARE PROVIDER for any injury you attribute to the INCIDENT? If so, for each HEALTH CARE PROVIDER state:";

    return [
        {
            question_id: 83,
            question_number_text: "6.4",
            question_number: 6.4,
            question_text: `${question_text}`,
            question_section_id: "1",
            question_section: "(a)",
            question_section_text: "the name, ADDRESS, and telephone number;",
        },
        {
            question_id: 84,
            question_number_text: "6.4",
            question_number: 6.4,
            question_text: `${question_text}`,
            question_section_id: "2",
            question_section: "(b)",
            question_section_text: "the type of consultation, examination, or treatment provided;",
            question_options: null,
        },
        {
            question_id: 85,
            question_number_text: "6.4",
            question_number: 6.4,
            question_text: `${question_text}`,
            question_section_id: "3",
            question_section: "(c)",
            question_section_text: "the dates you received consultation, examination, or treatment; and",
            question_options: null,
        },
        {
            question_id: 86,
            question_number_text: "6.4",
            question_number: 6.4,
            question_text: `${question_text}`,
            question_section_id: "4",
            question_section: "(d)",
            question_section_text: "the charges to date.",
            question_options: null,
        }
    ]
}

const defaultInitialDisclosureQuestions = () => {
    return [
      {
          "question_number": 1,
          "question_number_text": "1",
          "question_text": "The names, addresses, telephone numbers, and email addresses of all persons likely to have discoverable information, along with the subjects of that information, that the disclosing party may use to support their claims or defenses, or that is relevant to the subject matter of the action.",
          "question_id": 1,
          "question_options": null,
          "question_section_id": "1",
          "question_section": "(a)",
        "question_section_text": "The name, address, telephone number, and email address;",
        "question_type": "InitialDisclosure",
        // "is_consultation_set": 1,
        // "consultation_set_no": 1,
        // "is_duplicate": 0,
        // "duplicate_set_no": 1
          
      },
      {
          "question_number": 1,
          "question_number_text": "1",
          "question_text": "The names, addresses, telephone numbers, and email addresses of all persons likely to have discoverable information, along with the subjects of that information, that the disclosing party may use to support their claims or defenses, or that is relevant to the subject matter of the action.",
          "question_id": 2,
          "question_options": null,
          "question_section_id": "2",
          "question_section": "(b)",
          "question_section_text": "subject of discoverable information",
          "question_type": "InitialDisclosure",
        //   "is_consultation_set": 1,
        //   "consultation_set_no" : 1,
        //   "is_duplicate": 0,
        //   "duplicate_set_no": 1
      },
      {
          "question_number": 2,
          "question_number_text": "2",
          "question_text": "A copy, or a description by category and location, of all documents, electronically stored information, and tangible things that the disclosing party has in its possession, custody, or control and may use to support its claims or defenses, or that is relevant to the subject matter of the action.",
          "question_id": 3,
          "question_options": null,
          "question_section_id": "1",
          "question_section": "(a)",
          "question_section_text": "Description by category and cation of document, electronically stored information, or tangible thing.",
          "question_type": "InitialDisclosure",
        //   "is_consultation_set": 1,
        //   "consultation_set_no" : 1,
        //   "is_duplicate": 0,
        //   "duplicate_set_no": 1
      }
  ]
  }

export const defaultQuestion = (record) => {
    const documentType = record && record[0].document_type || false;
    if(documentType && documentType == 'IDC'){
        let data = record && record.length > 0 && defaultInitialDisclosureQuestions();
        const questionsRecord = record && record.length > 0 && record.sort((a,b) => a.consultation_set_no - b.consultation_set_no)
        const selectedQuestions = questionsRecord && questionsRecord[(questionsRecord && questionsRecord.length) - 1];
        const addSelectedQuestions =  data && data.length && data.filter((el) => el.question_number == selectedQuestions?.question_number);
        let questions;
        if (addSelectedQuestions && record) {
            questions =
            addSelectedQuestions &&
            addSelectedQuestions.map((el) =>
                    Object.assign({}, el, {
                        id: Date.now().toString(36) + Math.random().toString(36).substr(2),
                        consultation_set_no: selectedQuestions.consultation_set_no + 1,
                        is_consultation_set: 1,
                        subgroup: null,
                        question_type: selectedQuestions.question_type,
                        lawyer_response_text: null,
                        lawyer_response_status: "NotStarted",
                        client_response_text: null,
                        file_upload_status: null,
                        uploaded_documents: null,
                        legalforms_id: selectedQuestions.legalforms_id,
                        case_id: selectedQuestions.case_id,
                        client_id: selectedQuestions.client_id,
                        client_response_status: null,
                        document_type: selectedQuestions.document_type,
                        practice_id: selectedQuestions.practice_id,
                        sending_type: selectedQuestions.sending_type,
                        total_consultation_count: null,
                        party_id: selectedQuestions.party_id || false
                    })
                );
        }
        return questions;
    }else {
        let data = record && record.length > 0 && defaultQuestionsData(record[0]);
        let questions;
        if (data && record) {
            questions =
                data &&
                data.map((el) =>
                    Object.assign({}, el, {
                        id: Date.now().toString(36) + Math.random().toString(36).substr(2),
                        consultation_set_no: record[0].total_consultation_count + 1,
                        is_consultation_set: 1,
                        subgroup: null,
                        question_type: record[0].question_type,
                        lawyer_response_text: null,
                        lawyer_response_status: "NotStarted",
                        client_response_text: null,
                        file_upload_status: null,
                        uploaded_documents: null,
                        legalforms_id: record[0].legalforms_id,
                        case_id: record[0].case_id,
                        client_id: record[0].client_id,
                        client_response_status: null,
                        document_type: record[0].document_type,
                        practice_id: record[0].practice_id,
                        sending_type: record[0].sending_type,
                        total_consultation_count: null,
                        party_id: record[0].party_id || false
                    })
                );
        }
    
        return questions;
    }
    
};

export const consultationNote = (code) => {
    switch (code) {
        case "es": return `Si tiene más de una consulta, examen o tratamiento, por favor use "Agregar nueva consulta" al final de esta pregunta para registrar cada uno de ellos.`;
        case "vi": return `Nếu bạn có nhiều lần tư vấn, khám hoặc điều trị, vui lòng sử dụng "Thêm Tư vấn Mới" ở cuối câu hỏi này để ghi lại từng lần tư vấn.`
        case "en": return `If you have more than one consultation, examination or treatment, please use "Add New Consultation" at the end of this question to record each of them.`
    }
}