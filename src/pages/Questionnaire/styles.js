import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    questionnaire: {
        fontSize: '22px',
        marginBottom: theme.spacing(2),
    },
    backButton: {
        marginRight: theme.spacing(1),
    },
    instructions: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
        marginLeft: theme.spacing(8),
        marginRight: theme.spacing(8),
        textAlign: 'center',
    },
    thankyou: {
        marginBottom: theme.spacing(1),
        fontSize: '22px',
    },
    checkIcon: {
        width: '100%',
        height: '100px',
        color: '#2ca01c',
    },
    languageType: {
        padding: '2px 12px 2px 12px',
        border: '2px solid gray',
        borderRadius: '28px',
        paddingLeft: '20px',
        paddingRight: '30px',
        fontFamily: 'Avenir-Bold',
        outline: 'none',
        fontSize: '13px',
        position: 'relative',
        background: 'transparent'
    },
    btnPreview: {
        fontSize: '22px',
        width: '100%',
        height: '50px',
        margin: '10px',
        marginTop: '100px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        '&:hover': {
            background: '#2ca01c',
        }
    },
    btnContinue: {
        fontWeight: 'bold',
        textTransform: 'none !important',
        fontFamily: 'Avenir-Bold',
        backgroundColor: '#fff !important',
        boxShadow: 'none',
        color: '#47AC39',
        fontSize: '18px',
        paddingTop: '20px',
        paddingBottom: '2px',
        '&:hover': {
            boxShadow: 'none'
        }
    }
}));


export default useStyles;