/**
 * 
 * Introduction
 * 
 */

import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import styles from './styles';
import Button from '@material-ui/core/Button';
import withQueryParams from 'react-router-query-params';
import { compose } from 'redux';


function Introduction({ title, content, QuestionInstructions, handleContinue, queryParams = {} }) {
    const { TargetLanguageCode } = queryParams;
    const classes = styles();

    return (
        <Grid container item spacing={3} justify="center" className={classes.grid}>
            <Grid style={{ maxWidth: '600px', alignItems: 'center' }}>
                <Grid >
                    <Typography variant="h1" gutterBottom className={classes.title}>{title}</Typography>
                    <Typography className={classes.instructions} dangerouslySetInnerHTML={{ __html: content}} />
                    {QuestionInstructions ? <Typography className={classes.questionInstructions} dangerouslySetInnerHTML={{ __html: QuestionInstructions}} /> : null}
                    <Button className={classes.btnContinue}
                        type="button"
                        variant="contained"
                        color="primary"
                        onClick={handleContinue}
                    >
                        {TargetLanguageCode === 'es' ? 'Continuar' : TargetLanguageCode === 'vi' ? 'Tiếp tục' : 'Continue'}
                    </Button>
                </Grid>
            </Grid>
        </Grid>
    )
}

export default compose(
    withQueryParams()
)(Introduction);
