import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    title: {
        marginBottom: theme.spacing(2),
        fontSize: '32px',
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: '20px',
    },
    instructions: {
        marginTop: theme.spacing(5),
        marginBottom: theme.spacing(1),
        fontSize: '16px',
    },
    grid: {
        margin: '20px',
        width: '100%',
        marginRight: '30px',
        display: 'flex',
        alignItems: 'center',
        height: 'auto',
        minHeight: '580px',
    },
    gridContent: {
        width: '40%',
        marginLeft: theme.spacing(8),
        marginRight: theme.spacing(8),
    },
    btnContinue: {
        fontSize: '22px',
        width: '100%',
        height: '50px',
        margin: '10px',
        marginTop: '100px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        '&:hover': {
            background: '#2ca01c',
        }
    },
    questionInstructions: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(1),
        fontSize: '16px',
    }
}));


export default useStyles;