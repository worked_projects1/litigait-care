import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Questionnaire from './pages/Questionnaire/Questionnaire';
import TermsAcceptance from './pages/TermsAcceptance/TermsAcceptance';
import HomePage from './pages/HomePage';
import TekSign from './pages/TekSign/TekSign';
import NotFound from './pages/NotFound/NotFound';
import Viewer from './pages/Viewer/Viewer';

//Material UI Theme Provider
import { ThemeProvider } from '@material-ui/core/styles';
import theme from './theme';
import './fonts.css';
import './App.css';
import { Provider } from 'react-redux';
import { createStore, combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

//React Clear Cache
import { ClearCacheProvider } from 'react-clear-cache';

const reducers = {
  form: formReducer
};
const reducer = combineReducers(reducers);
const store = createStore(reducer);

const App = ({ history }) => (
  <ClearCacheProvider duration={5000}>
    <ThemeProvider theme={theme}>
      <Provider store={store}>
        <Router>
          <Switch>
            {/* <Route exact path='/' component={NotFound} /> */}
            <Route path='/questionnaire/:token' component={Questionnaire} />
            <Route path='/lawyer-answer-verification/:token' component={TekSign} />
            <Route path='/hipaa-terms/:clientId/:caseId' component={TermsAcceptance} />
            <Route path='/fee-terms/:clientId/:caseId' component={TermsAcceptance} />
            <Route path='/view-pdf' component={Viewer} />
            <Route path="*" component={HomePage} />
          </Switch>
        </Router>
      </Provider>
    </ThemeProvider>
  </ClearCacheProvider>
);

export default App;
