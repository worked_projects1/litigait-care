/**
 * 
 * Terms 
 * 
 */


import React from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import styles from './styles';
import Button from '@material-ui/core/Button';


function Terms({ title, value, content, handleClick }) {
    const classes = styles();

    const handleContent = (type) => {
        var elmnt = document.getElementById("content");
        elmnt.scrollTo(0, 0);
        handleClick(type, title, value);
    }


    return (
        <Grid container item spacing={3} justify="center" className={classes.grid}>
            <Grid className={classes.gridWidth}>
                <Typography variant="h1" gutterBottom className={classes.title}>{title}</Typography>

                <Grid id="content" className={classes.gridContent}>
                    <Typography className={classes.instructions}>{content.split('\n').map((line, pos) => pos === 0 ? <span key={pos}>{line}</span> : <p key={pos}>{line}</p>)}</Typography>
                </Grid>
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6} className={classes.btnGrid}>
                        <Button className={classes.btnContinue}
                            type="button"
                            variant="contained"
                            color="primary"
                            onClick={() => handleContent("accept")}
                        >
                            Accept
                    </Button>
                    </Grid>
                    <Grid item xs={12} md={6} className={classes.btnGrid}>
                        <Button className={classes.btnDoNotAccept}
                            type="button"
                            variant="contained"
                            color="primary"
                            onClick={() => handleContent("not_accept")}
                        >
                            I Do Not Accept
                    </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    )

}


export default Terms;