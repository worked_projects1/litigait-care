import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    title: {
        marginBottom: theme.spacing(2),
        fontSize: '32px',
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: '20px',
    },
    instructions: {
        marginBottom: theme.spacing(1),
        fontSize: '16px',
        marginLeft: '10px',
        marginRight: '10px',
        minWidth: '300px',
        minHeight: '500px',
    },
    grid: {
        margin: '20px',
        width: '100%',
        marginRight: '30px',
        display: 'flex',
        alignItems: 'center',
        height: 'auto',
        minHeight: '580px',
    },
    gridWidth: {
        maxWidth: '600px',
        alignItems: 'center',
    },
    gridContent: {
        maxHeight: '340px',
        overflow: 'auto',
        marginBottom: '20px',
    },
    btnContinue: {
        fontSize: '18px',
        height: '46px',
        margin: '10px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        textTransform: 'none',
        minWidth: '220px',
        '&:hover': {
            background: '#2ca01c',
        }
    },
    btnDoNotAccept: {
        fontSize: '18px',
        height: '46px',
        margin: '10px',
        fontWeight: 'bold',
        backgroundColor: '#EC8733',
        textTransform: 'none',
        minWidth: '220px',
        '&:hover': {
            background: '#EC8733',
        }
    },
    btnGrid: {
        textAlign: 'center',
    }
}));


export default useStyles;