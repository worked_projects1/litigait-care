/**
 * 
 *  Modal Dialog
 * 
 */

import React, { useState } from 'react';
import CloseIcon from '@material-ui/icons/Close';
import { Backdrop, Modal, Paper, Fade, Grid, Typography, Button } from '@material-ui/core';
import Styles from './styles';
 
function ModalDialog({ children, className, style, headerStyle, footerStyle, show, onClose, heading, Label, CancelLabel, handleSubmit, disabled }) {
    const classes = Styles();
    const [showModal, setModalOpen] = useState(false);    
 
    const handleClose = () => {
        setModalOpen(false);
        if (onClose) {
            onClose();
        }
    }

    return <Grid className={className} style={style}>
        {children && children(() => setModalOpen(!showModal))}
        <Modal
            aria-labelledby="transition-modal-title"
            aria-describedby="transition-modal-description"
            open={showModal || show || false}
            className={classes.modal}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
                timeout: 500,
            }}>
            <Fade in={showModal || show}>
                <Paper className={classes.paper}>
                    <Grid container justify="flex-end" className={classes.close}>
                        <CloseIcon onClick={handleClose} className={classes.closeIcon} />
                    </Grid>
                    {typeof heading === 'function' && heading() || heading && <Grid className={classes.gridHeader} style={headerStyle || {}}>
                        <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
                    </Grid> || null}
                    <Grid container justify="flex-end" style={footerStyle}>
                        <Button
                            type="button"
                            variant="contained"
                            color="primary"
                            className={classes.button}
                            disabled={disabled}
                            onClick={handleSubmit}>
                            {Label || 'Send'}
                        </Button>
                        <Button
                            type="button"
                            variant="contained"
                            onClick={handleClose}
                            className={classes.button}>
                            {CancelLabel || 'Cancel'}
                        </Button>
                    </Grid> 
                </Paper>
            </Fade>
        </Modal>
    </Grid>
}
 
 
export default ModalDialog;