

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    paper: {
        margin: '0px 15px',
        backgroundColor: theme.palette.background.paper,
        border: 'none',
        boxShadow: theme.shadows[5],
        padding: '12px',
        outline: 'none',
        paddingBottom: '35px',
        width: '540px'
    },
    button: {
        fontWeight: 'bold',
        textTransform: 'none',
        fontFamily: 'Avenir-Regular',
        marginTop: '10px',
        outline: 'none',
        textTransform: 'capitalize',
        marginRight: '12px'
    },
    closeIcon: {
        cursor: 'pointer'
    },
    gridHeader: {
        textAlign: 'center',
        margin: '5px',
        paddingLeft: '10px',
        paddingRight: '10px',
        maxWidth: '600px',
        maxHeight: '400px',
        overflow: 'auto'
    }
}));


export default useStyles;