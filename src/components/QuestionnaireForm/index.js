/**
 * 
 * Questionnaire Form
 * 
 */


import React, { useState, useEffect } from 'react';
import { Grid, Radio, RadioGroup, FormControlLabel, FormControl } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import styles from './styles';
import InputField from '../InputField';
import CheckboxField from '../CheckboxField';
import { Field, FieldArray, reduxForm } from 'redux-form';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import withQueryParams from 'react-router-query-params';
import { compose } from 'redux';
import UploadField from '../UploadField';
import AlertDialog from '../AlertDialog';
import lodash from 'lodash';
import AddCircleRoundedIcon from '@material-ui/icons/AddCircleRounded';
import { ReactComponent as Trash } from '../../images/trash.svg';
import { ClipLoader } from 'react-spinners';

const QuestionsField = ({ records, fields, metaData, classes, activeStep, btnLabel, TargetLanguageCode, addConsultationFunc, deleteConsultation, consultationNote, loader, initialize, meta: { touched, error } }) => {

    let consultation = records && records[0] && records[0].is_consultation_set || false;
    let consultationSet = records && records.length > 0 && records[0] && records[0]['consultation_set_no'] && lodash.groupBy(records, 'consultation_set_no') || [];
    let subQuestionSet = Object.keys(consultationSet).map((el, i) => consultationSet[el]);

    let StaticLabel = `** Here is the draft response from the attorney for your review. Please make any modifications as required **`;

    const handleFocus = (id) => {
        document.getElementById(id).focus()
    }

    return (
        <Grid container>
            <Grid item xs={12}>
                {records && records.length > 0 && records[0].question_section_text ?
                    <Typography variant="subtitle2" className={classes.contentPadding}>
                        <b>{records[0]['question_number_text']}. {records[0]['document_type'] == 'FROGS' ? (records[0]['question_text']).split('\n').map((line, pos) => pos == 0 ? <span key={pos}>{line}</span> : <p key={pos} className={classes.line}>{line}</p>) : records[0]['question_text']}</b>
                    </Typography> : null}
            </Grid>
            {consultation &&
                <Grid item xs={12}>
                    <Typography variant="subtitle2">
                        <b>{TargetLanguageCode === 'es' ? 'Nota: ' : TargetLanguageCode === 'vi' ? 'Ghi chú: ' : 'Note: '}</b>{records && records.length && records[0].document_type === "IDC" ? "If you have more than one, please click 'Add New' at the end of this question to record each of them." : consultationNote}
                    </Typography>
                </Grid>}
            {records && records[0] && records[0]?.document_type && records[0]?.document_type.toLowerCase() === 'odd' && records[0]?.question_category ? <Typography style={{ marginTop: '10px', textDecoration: 'underline', fontWeight: 'bold', textTransform: 'capitalize' }}>{`Category - ${records[0]?.question_category.toLowerCase()}`}</Typography> : null}
            {consultation && subQuestionSet ? (Object.keys(subQuestionSet) || []).map((key) => {
                return <React.Fragment>
                    <Typography variant="subtitle2" className={subQuestionSet && subQuestionSet[key][0].consultation_set_no !== 1 ? classes.consultation : classes.contentPadding}><b>{TargetLanguageCode === 'es' ? 'Consulta' : TargetLanguageCode === 'vi' ? 'Tham vấn' : records && records.length && records[0].document_type === "IDC" ? 'Person' : 'Consultation'} {parseInt(key) + 1}</b></Typography>
                    {parseInt(key) > 0 && subQuestionSet && subQuestionSet[key][0].consultation_set_no !== 1 && !subQuestionSet[key].some(_ => _.lawyer_response_text && _.lawyer_response_text != "") && <Typography component="span" variant="subtitle2" className={classes.deleteConsultion}>
                        <AlertDialog
                            onConfirm={() => deleteConsultation(subQuestionSet[key], initialize)}
                            onConfirmPopUpClose={true}
                            btnLabel1={TargetLanguageCode === 'es' ? 'SÍ' : TargetLanguageCode === 'vi' ? 'VÂNG' : 'YES'}
                            btnLabel2={TargetLanguageCode === 'es' ? 'NO' : TargetLanguageCode === 'vi' ? 'KHÔNG' : 'NO'}
                            header={true}
                            description={TargetLanguageCode === 'es' ? '¿Quieres eliminar esta consulta?' : TargetLanguageCode === 'vi' ? 'Bạn có muốn xóa tư vấn này không?' : records && records.length && records[0].document_type === "IDC" ? 'Do you want to delete this question set?' : 'Do you want to delete this consultation?'}>
                            {(open) =>
                                <Trash className={classes.attachDelete} onClick={open} />}
                        </AlertDialog>
                    </Typography>}
                    <Grid item xs={12}>
                        <Grid container>
                            {subQuestionSet && subQuestionSet[key] && subQuestionSet[key].length > 0 && subQuestionSet[key].map((field, index) => {
                                const record = field || {};
                                const {
                                    question_section,
                                    document_type,
                                    question_section_text,
                                    question_number_text,
                                    question_text,
                                    file_upload_status,
                                    sending_type,
                                    uploaded_documents,
                                    checked,
                                    consultation_set_no,
                                    share_attorney_response,
                                    client_response_text,
                                    lawyer_response_text
                                } = record;

                                const upload_status = file_upload_status === 1 && sending_type == 'selected_questions' && uploaded_documents && uploaded_documents.length === 0;

                                return (<React.Fragment key={index}>
                                    <Grid item xs={12}>
                                        <Typography variant="subtitle2" style={{ padding: '12px 0px 12px 0px' }}>
                                            {question_section ? <b>{question_section} {document_type === 'FROGS' ? (question_section_text).split('\n').map((line, pos) => pos === 0 ? <span key={pos}>{line}</span> : <p key={pos} style={{ margin: '4px' }}>{line}</p>) : (question_section_text)}</b>
                                                :
                                                <b>{question_number_text}. {document_type === 'FROGS' ? (question_text).split('\n').map((line, pos) => pos === 0 ? <span key={pos}>{line}</span> : <p key={pos} style={{ margin: '4px' }}>{line}</p>) : (question_text)}</b>}
                                        </Typography>
                                    </Grid>
                                    <Grid item xs={12}>
                                        <Field
                                            name={`questions[${key}][${index}].client_response_text`}
                                            label=""
                                            type="text"
                                            id={record.id}
                                            metaData={metaData}
                                            className={classes.fieldColor}
                                            component={InputField}
                                            StaticLabel={(records[index] && !records[index].client_response_text && lawyer_response_text && share_attorney_response) ? () => (<span className={classes.textSize} onClick={() => handleFocus(record.id)}>{StaticLabel}</span>) : null} />
                                    </Grid>
                                    {document_type === 'RFPD' || document_type === 'rfpd' ?
                                        <Grid item xs={12}>
                                            {(upload_status && !checked) || (uploaded_documents && uploaded_documents.length > 0) || sending_type == 'all' ? <Field name={`questions[${index}][${consultation_set_no}].uploaded_documents`} validate={[value => (value && value.length > 0) || sending_type == 'all' ? undefined : 'Files Requested']} label="" type="text" file_upload_status={file_upload_status} sending_type={sending_type} className={classes.fieldColor} component={UploadField} /> : null}
                                            {upload_status ?
                                                <Grid container style={{ alignItems: 'center' }}>
                                                    <Field name={`questions[${index}][${consultation_set_no}].checked`} label={<span style={{ fontSize: '14px', fontWeight: 700 }}>I don't have files</span>} style={{ color: "#2ca01c" }} type="checkbox" metaData={metaData} component={CheckboxField} />
                                                </Grid> : null}
                                        </Grid> : null}
                                </React.Fragment>)
                            }) || null}
                        </Grid>
                    </Grid>
                </React.Fragment>
            }) :
                <Grid item xs={12}>
                    <Grid container>
                        {fields && fields.length > 0 && fields.map((field, index) => {
                            const record = fields.get(index) || {};
                            const {
                                question_section,
                                question_number,
                                question_number_text,
                                document_type,
                                question_section_text,
                                question_text,
                                file_upload_status,
                                sending_type,
                                uploaded_documents,
                                checked,
                                share_attorney_response,
                                client_response_text,
                                lawyer_response_text
                            } = record;

                            const upload_status = file_upload_status === 1 && sending_type == 'selected_questions' && uploaded_documents && uploaded_documents.length === 0;

                            return (<React.Fragment key={index}>
                                <Grid item xs={12}>
                                    <Typography variant="subtitle2" style={{ padding: '12px 0px 12px 0px' }}>
                                        {question_section ? <b>{question_section} {document_type === 'FROGS' ? (question_section_text).split('\n').map((line, pos) => pos === 0 ? <span key={pos}>{line}</span> : <p key={pos} style={{ margin: '4px' }}>{line}</p>) : (question_section_text)}</b>
                                            :
                                            <b>{question_number_text}. {document_type === 'FROGS' ? (question_text).split('\n').map((line, pos) => pos === 0 ? <span key={pos}>{line}</span> : <p key={pos} style={{ margin: '4px' }}>{line}</p>) : (question_text)}</b>}
                                    </Typography>
                                </Grid>
                                <Grid item xs={12}>
                                    <Field
                                        name={`${field}.client_response_text`}
                                        label=""
                                        type="text"
                                        id={record.id}
                                        metaData={metaData}
                                        className={classes.fieldColor}
                                        component={InputField}
                                        StaticLabel={(records[index] && !records[index].client_response_text && lawyer_response_text && share_attorney_response) ? () => (<span className={classes.textSize} onClick={() => handleFocus(record.id)}>{StaticLabel}</span>) : null} />
                                </Grid>
                                {document_type === 'RFPD' || document_type === 'rfpd' ?
                                    <Grid item xs={12}>
                                        {(upload_status && !checked) || (uploaded_documents && uploaded_documents.length > 0) || sending_type == 'all' ? <Field name={`${field}.uploaded_documents`} validate={[value => (value && value.length > 0) || sending_type == 'all' ? undefined : 'Files Requested']} label="" type="text" file_upload_status={file_upload_status} sending_type={sending_type} className={classes.fieldColor} component={UploadField} /> : null}
                                        {upload_status ?
                                            <Grid container style={{ alignItems: 'center' }}>
                                                <Field name={`${field}.checked`} label={<span style={{ fontSize: '14px', fontWeight: 700 }}>I don't have files</span>} style={{ color: "#2ca01c" }} type="checkbox" metaData={metaData} component={CheckboxField} />
                                            </Grid> : null}
                                    </Grid> : null}
                            </React.Fragment>)
                        }) || null}
                    </Grid>
                </Grid>
            }
            {consultation ? <span className={classes.consultBtn} onClick={!loader ? () => addConsultationFunc(subQuestionSet) : null}>
                <AddCircleRoundedIcon style={{ fill: "#2ca01c", cursor: 'pointer', verticalAlign: 'middle' }} />
                <Button type="button">{loader ? <ClipLoader color={"#2ca01c"} size={24} /> : btnLabel}</Button>
            </span> : null}
        </Grid>)
}

function QuestionnaireForm(props) {
    const { handleSubmit, metaData = {}, activeStep, steps, handleBack, queryParams = {}, records, invalid, handleSkip, handleSkipQuestion, formValues, initialize, skipOptions, handleAddConsultation, deleteConsultation, consultationNote } = props;
    const { TargetLanguageCode } = queryParams;
    const classes = styles();
    const [value, setValue] = useState(false);
    const [loader, setLoader] = useState(false);

    const handleChange = (e) => {
        setValue(e.target.value);
    }

    const addConsultationFunc = (subQuestionSet) => {
        setLoader(true);
        handleAddConsultation(subQuestionSet, setLoader);
    }

    return (
        <form onSubmit={handleSubmit}>
            <Grid container>
                <Grid container item spacing={2}>
                    <Grid item xs={12}>
                        <FieldArray
                            name={'questions'}
                            metaData={metaData}
                            classes={classes}
                            component={QuestionsField}
                            records={records}
                            activeStep={activeStep}
                            btnLabel={TargetLanguageCode === 'es' ? 'Agregar nueva consulta' : TargetLanguageCode === 'vi' ? 'Thêm tư vấn mới' : records && records.length && records[0].document_type === "IDC" ? 'Add new' : 'Add new consultation'}
                            TargetLanguageCode={TargetLanguageCode}
                            deleteConsultation={deleteConsultation}
                            consultationNote={consultationNote}
                            addConsultationFunc={addConsultationFunc}
                            loader={loader}
                            initialize={initialize} />
                    </Grid>
                </Grid>
                <Grid container spacing={2} direction="row" justify="flex-end" alignItems="center" className={classes.btnGrid}>
                    {activeStep !== 0 ?
                        <Button
                            className={classes.backBtn}
                            onClick={handleBack}
                            variant="contained"
                            color="black">
                            {TargetLanguageCode === 'es' ? 'Espalda' : TargetLanguageCode === 'vi' ? 'Lưng' : 'Back'}
                        </Button> : null}
                    {((records && records.find(_ => _.client_response_text && _.client_response_status === "ClientResponseAvailable"))) ?
                        <Button
                            className={classes.skipBtn}
                            onClick={handleSkip}
                            variant="contained"
                            color="black">
                            {TargetLanguageCode === 'es' ? 'Saltar' : TargetLanguageCode === 'vi' ? 'Nhảy' : 'Skip'}
                        </Button>
                        :
                        <AlertDialog
                            onConfirm={() => handleSkipQuestion(records, value, initialize)}
                            onConfirmPopUpClose={true}
                            btnLabel1={TargetLanguageCode === 'es' ? 'OK' : TargetLanguageCode === 'vi' ? 'VÂNG' : 'OK'}
                            btnLabel2={TargetLanguageCode === 'es' ? 'CANCELAR' : TargetLanguageCode === 'vi' ? 'SỰ HỦY BỎ' : 'CANCEL'}
                            heading={() => (
                                <Grid container direction="row" alignItems="center" className={classes.skipContainer}>
                                    <FormControl>
                                        <Grid className={classes.skipHeading}>{TargetLanguageCode === 'es' ? '¿Por qué desea omitir esta pregunta?' : TargetLanguageCode === 'vi' ? 'Tại sao bạn muốn bỏ qua câu hỏi này?' : 'Why would you like to skip this question?'}</Grid>
                                        <RadioGroup
                                            aria-labelledby="demo-radio-buttons-group-label" name="radio-buttons-group"
                                            value={value || ''}
                                            onChange={handleChange}
                                        >
                                            {skipOptions && skipOptions.map(el =>
                                                <FormControlLabel
                                                    key={el}
                                                    value={el}
                                                    control={<Radio style={{ color: "grey" }} />}
                                                    label={<span className={classes.textSize}>{el}</span>} />
                                            )}
                                        </RadioGroup>
                                    </FormControl>
                                </Grid>
                            )}>
                            {(open) =>
                                <Button
                                    className={classes.skipBtn}
                                    onClick={open}
                                    variant="contained"
                                    color="black">{TargetLanguageCode === 'es' ? 'Saltar' : TargetLanguageCode === 'vi' ? 'Nhảy' : 'Skip'}
                                </Button>}
                        </AlertDialog>}
                    <Button
                        className={classes.saveBtn}
                        type="submit"
                        disabled={invalid}
                        variant="contained"
                        color="primary">
                        {activeStep === steps.length - 1 ? (TargetLanguageCode === 'es' ? 'Salvar & Terminar' : TargetLanguageCode === 'vi' ? 'Sự bắt banh lại & Sự hết' : 'Save & Finish') : TargetLanguageCode === 'es' ? 'Salvar & Continuar' : TargetLanguageCode === 'vi' ? 'Sự bắt banh lại & Tiếp tục' : 'Save & Continue'}
                    </Button>
                </Grid>
            </Grid>
        </form>
    );
}

function mapStateToProps({ form }, props) {
    const { records = [] } = props;
    const formValues = form[props.form] && form[props.form].values || false;
    let formRecord = records.map(r => Object.assign({}, r, { uploaded_documents: r.uploaded_documents && typeof r.uploaded_documents === 'string' ? JSON.parse(r.uploaded_documents) : [] }));
    var formData = [];
    let consFormRecord = [];
    let consFinalFormRecord;

    formValues && formValues['questions'] && formValues['questions'].length > 0 && Object.keys(formValues['questions'] || []).map(key => {
        var split = Object.assign([], formValues['questions'][key]);
        formData.push(...split);
    });

    function convertHTMLEntity(text) {
        const span = document.createElement('span');
        return text.replaceAll(/&[#A-Za-z0-9]+;/gi, (entity, position, text) => {
            span.innerHTML = entity;
            return span.innerText;
        });
    }

    formRecord = formRecord.map(e => {
        if (!e.client_response_text && e.lawyer_response_text && e.share_attorney_response) {
            let client_response_text = e.lawyer_response_text && e.lawyer_response_text.replace(/<[^>]*?>/g, '');
            client_response_text = client_response_text && client_response_text.replaceAll('&nbsp;', '');
            return Object.assign({}, e, { client_response_text: convertHTMLEntity(client_response_text) || null });
        } else {
            return Object.assign({}, e);
        }
    });

    let formValue = formValues && formValues['questions'] && formValues['questions'].map(e => {
        if (e.client_response_text && e.lawyer_response_text && e.share_attorney_response) {
            let filterFormRecord = formRecord.filter(el => el.id === e.id);
            if (filterFormRecord && filterFormRecord[0] && filterFormRecord[0].client_response_text !== e.client_response_text) {
                return Object.assign({}, e, { client_modified_response: true });
            } else {
                return Object.assign({}, e, { client_modified_response: false });
            }
        } else {
            return Object.assign({}, e);
        }
    });

    const filterRecord = formData && formData.length > 0 && formRecord && formRecord.length > 0 && formRecord.filter(r => !formData.some(el => r && r.id === el.id)) || [];
    const finalRecord = filterRecord && filterRecord.length > 0 && formData && formData.length > 0 && formData.concat(filterRecord) || formValues && formValues['questions'];

    const consultQues = finalRecord && finalRecord && finalRecord[0] && finalRecord[0].is_consultation_set && finalRecord[0].is_consultation_set == 1 && finalRecord || [];


    const groupedQues = consultQues && Array.isArray(consultQues) && consultQues.length > 0 && consultQues[0].is_consultation_set && lodash.groupBy(consultQues, 'consultation_set_no');
    const groupedQuesArray = groupedQues && Object.keys(groupedQues).map((el, i) => Object.assign({}, groupedQues[el]));

    const formRecords = groupedQues ? { questions: groupedQuesArray } : formValues;

    if (formRecords && formRecords['questions'] && formRecords['questions'][0] && formRecords['questions'][0][0] && formRecords['questions'][0][0].is_consultation_set && formRecords['questions'][0][0].is_consultation_set == 1) {
        formRecords['questions'].map(arr => {
            let split = Object.keys(arr).map(i => arr[i]);
            consFormRecord.push(...split);
        });
        consFinalFormRecord = consFormRecord.map(e => {
            if (e.client_response_text && e.lawyer_response_text && e.share_attorney_response) {
                let filterFormRecord = formRecord.filter(el => el.id === e.id);
                if (filterFormRecord && filterFormRecord[0] && filterFormRecord[0].client_response_text !== e.client_response_text) {
                    return Object.assign({}, e, { client_modified_response: true });
                } else {
                    return Object.assign({}, e, { client_modified_response: false });
                }
            } else {
                return Object.assign({}, e);
            }
        })
    }

    const groupedQues1 = consFinalFormRecord && Array.isArray(consFinalFormRecord) && consFinalFormRecord.length > 0 && consFinalFormRecord[0].is_consultation_set && lodash.groupBy(consFinalFormRecord, 'consultation_set_no');
    const groupedQuesArray1 = groupedQues1 && Object.keys(groupedQues1).map((el, i) => Object.assign({}, groupedQues1[el]));

    const consultationFormRecords = groupedQuesArray1 && Object.keys(groupedQuesArray1).length > 0 && { questions: groupedQuesArray1 };


    return {
        initialValues: consultationFormRecords && Object.keys(consultationFormRecords).length > 0 ? consultationFormRecords : formValue && formValue.length > 0 ? Object.assign({}, { questions: formValue }) : Object.assign({}, { questions: formRecord }),
        formValues: formData && formData.length > 0 ? formData : formValue || []
    }
}

export default compose(
    connect(mapStateToProps),
    reduxForm({
        form: 'questionnaire',
        enableReinitialize: true,
        destroyOnUnmount: false,
        touchOnChange: true
    }),
    withQueryParams()
)(QuestionnaireForm);