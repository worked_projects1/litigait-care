import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    fieldColor: {
        background: 'rgba(0, 0, 0, 0.09)',
        color: 'black !important',
        height: '100px',
        overflowY: 'auto',
        padding: '10px',
        fontSize: '12px',
        borderBottom: '2px solid #2ca01c',
        cursor: 'pointer'
    },
    blackColor: {
        background: 'rgba(0, 0, 0, 0.09)',
        color: 'black !important',
        height: '100px',
        overflowY: 'auto',
        padding: '10px',
        fontSize: '12px',
        borderBottom: '2px solid black',
        cursor: 'pointer'
    },
    blueColor: {
        background: 'rgba(0, 0, 0, 0.09)',
        color: 'black !important',
        height: '100px',
        overflowY: 'auto',
        padding: '10px',
        fontSize: '12px',
        borderBottom: '2px solid #3275BF',
        cursor: 'pointer'
    },
    btn: {
        fontSize: '12px',
        textTransform: 'none',
        padding: '4px 8px',
        margin: '6px 6px',
        fontWeight: 'bold',
    },
    btnGrid: {
        marginTop: '6px',
        marginBottom: '6px',
    },
    questionnaire: {
        fontSize: '22px',
    },
    saveBtn: {
        fontSize: '14px !important',
        padding: '5px 20px !important',
        margin: '10px !important',
        marginLeft: '20px !important',
        marginRight: '0px !important',
        fontWeight: 'bold !important',
        backgroundColor: '#2ca01c',
        textTransform: 'none !important',
        color: 'white !important'
    },
    backBtn : {
        fontSize: '14px',
        padding: '5px 20px',
        margin: '10px',
        marginLeft: '20px',
        marginRight: '0px',
        fontWeight: 'bold',
        backgroundColor: '#D9DBE2',
        textTransform: 'none'
    },
    skipBtn: {
        fontSize: '14px',
        padding: '5px 20px',
        margin: '10px',
        marginLeft: '20px !important',
        marginRight: '0px',
        fontWeight: 'bold',
        backgroundColor: '#D9DBE2',
        textTransform: 'none'
    },
    textSize: {
        fontSize: '14px',
    },
    skipContainer: {
        padding: '20px',
    },
    skipHeading: {
        margin: '15px 0 5px 0'
    },
    deleteConsultion: {
        padding: '10px',
        paddingBottom: '0px',
        paddingTop: '26px'
    },
    attachDelete: {
        width: '20px',
        height: '20px',
        paddingLeft: '1px',
        marginBottom: '4px',
        cursor: 'pointer'
    },
    contentPadding: {
        padding: '12px 0px'
    },
    consultation:{
        paddingTop: '28px',
        paddingBottom: '12px'
    },
    consultBtn:{
        marginTop: '8px'
    },
    textSize: {
        fontSize: '14px',
        padding: 0,
    },
}));


export default useStyles;