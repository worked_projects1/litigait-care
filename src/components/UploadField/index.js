
/**
 * 
 * UploadField
 * 
 * 
 */
import React, { useState, useCallback } from 'react';
import { useDropzone } from 'react-dropzone';
import { Grid } from '@material-ui/core';
import { getContentType } from './utils';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import AddIcon from '@material-ui/icons/Add';
import PublishIcon from '@material-ui/icons/Publish';
import styles from './styles';
import { uploadFile, getSignature } from '../../Utils/api';
import Snackbar from '../Snackbar';
import { withStyles } from '@material-ui/core/styles';
import LinearProgress from '@material-ui/core/LinearProgress';
import ProgressProvider from '../ProgressProvider';
import axios from 'axios';


const BorderLinearProgress = withStyles((theme) => ({
  root: {
    height: 10,
    borderRadius: 5,
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    }
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[theme.palette.type === 'light' ? 200 : 700],
  },
  bar: {
    borderRadius: 5,
    backgroundColor: '#2ca01c',
  },
}))(LinearProgress);

function UploadField({ input, file_upload_status, sending_type, meta: { touched, error, warning } }) {
  const inputValue = input && input.value.length > 0 && Array.isArray(input.value) && input.value || [];
  const [uploadError, setuploadError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [showDownloadingProgress, setShowDownloadProgress] = useState(false);
  const [uploadingFileCount, setUploadingFileCount] = useState(false);
  const [uploadPercentage, setUploadPercentage] = useState(0);
  let maxFiles = 30;
  const classes = styles();

  const [file, setFile] = useState([]);
  const uploadingFilesCount = [...new Set(file)];
  const pushUniqueValue = (value) => {
    if (!file.includes(value)) {
      setFile(prevFiles => [...prevFiles, value]);
    }
  };

  const onUpload = async (files) => {
    let uploadLimit = files && files.length + inputValue.length <= maxFiles;
    let uploadedFiles = 0;
    if (uploadLimit) {
      const uploadedFileLength = files && files.length || 0;
      setShowDownloadProgress(true);
      setUploadingFileCount(uploadedFileLength);
      setLoading(true);
      const promises = await files.map(async (file, i) => {
        const Upload = getSignature;
        const fileName = file.name;
        const fileContentType = getContentType(fileName);
        return await Promise.resolve(Upload(fileName, fileContentType)
          .then(async (result) => {
            if (result) {
              return await axios.put(result.uploadURL, file, {
                headers: {
                  Accept: 'application/json',
                  'Content-Type': fileContentType,
                },
                onUploadProgress: (e) => {
                  pushUniqueValue(fileName);
                  setUploadPercentage(Math.round((e.loaded / e.total) * 100))
                },
              }).then(() => {
                return result;
              })
            }
          }))
      });

      Promise.all(promises).then((data) => {
        input.onChange(inputValue.concat(data));
        setLoading(false);
      }).finally(() => {
        setFile([]);
        setUploadPercentage(0);
        setShowDownloadProgress(false);
        setUploadingFileCount(false);
      });

    } else if (files) {
      setuploadError(`You can upload upto ${maxFiles} files(jpg, png, pdf) for this question`);
    }

  }

  const onDrop = useCallback(async (uploadedFiles) => {

    const acceptedFiles = uploadedFiles.map(file => {
      const fileName = file.name.replace(/[^a-zA-Z.0-9]/g, "");

      return new File([file], fileName, { type: file.type, lastModified: file.lastModified });
    });

    await onUpload(acceptedFiles)
  }, [onUpload]);

  const onDropRejected = useCallback((error) => {
    setuploadError(error[0].errors[0].message);
  }, []);

  const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject } = useDropzone({ accept: "image/*,application/pdf", multiple: true, onDrop, onDropRejected, disabled: loading });

  const thumbs = inputValue.map((file, i) => {
    return (
      <div className={classes.thumb} key={file}>
        <span className={classes.remove}>
          <HighlightOffIcon style={{ fill: "#2ca01c" }} onClick={() => input.onChange(inputValue.filter((f, key) => key !== i))} />
        </span>
        <div className={classes.thumbInner}>
          {["image/png", "image/jpeg"].includes(getContentType(file.s3_file_key)) ? (
            <div>
              <img src={file.public_url} alt="No image" className={classes.img} />
            </div>
          ) : getContentType(file.s3_file_key) === "application/pdf" ? (
            <div>
              <img src={`https://www.sat7uk.org/wp-content/uploads/2018/11/kisspng-pdf-computer-icons-encapsulated-postscript-logo-pdf-5afde5cc758a98.9626072515265888764815.png`} className={classes.img} />
            </div>
          ) : null}
        </div>
      </div>
    );
  });

  return (
    <Grid container justify="center" className={classes.root}>
      <Grid className={classes.paper}>
        {showDownloadingProgress ? <Grid>
          <ProgressProvider values={input.value ? [100] : inputValue && !uploadError ? [0, 50, 100] : [0]}>
            {percentage => (<BorderLinearProgress variant="determinate" value={uploadPercentage} />)}
          </ProgressProvider>
          <Grid style={{ paddingTop: '5px' }}>
            <span style={{ fontSize: '13px' }}>{`Uploading ${uploadingFilesCount.length || 1} of ${uploadingFileCount}`}</span>
          </Grid>
        </Grid> : null}
        <Grid className={classes.container}>
          {inputValue.length > 0 ? (
            <Grid {...getRootProps({ className: "dropzone" })} style={{ outline: "none" }}>
              <input {...getInputProps()} />
              <PublishIcon className={classes.upload} />
            </Grid>
          ) : (
            <Grid className={classes.firstDropzone} {...getRootProps()} style={{ outline: "none" }}>
              <input {...getInputProps()} />
              {isDragAccept && <p>jpg, png, pdf only accepted</p>}
              {isDragReject && <p>Some files will be rejected</p>}
              {!isDragActive &&
                <p>Upload Files ({file_upload_status === 1 && sending_type === 'selected_questions' ? `Requested for this question` : `Optional`})</p>}
              <p style={{ marginTop: '0px', marginBottom: '16px' }}>You can upload upto {maxFiles} files (jpg, png, pdf) for this question</p>
              <AddIcon />
            </Grid>
          )}
        </Grid>
        <Grid className={classes.documentPreview}>
          <aside className={classes.thumbsContainer}>{thumbs}</aside>
        </Grid>
      </Grid>
      <Grid item xs={12} className={classes.error}>
        {(error && <span>{error}</span>) || (warning && <span>{warning}</span>)}
      </Grid>
      <Snackbar show={uploadError ? true : false} text={uploadError} severity={'error'} handleClose={() => setuploadError(false)} />
    </Grid>
  );
}

export default UploadField;
