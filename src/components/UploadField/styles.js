import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: '16px',
    '@global': {
      '.MuiTypography-h6': {
        fontWeight: 600
      }
    }
  },
  upload: {
    boxShadow: 'none',
    width: '89px',
    height: '89px',
    zIndex: '100',
    padding: '5px',
    outline: 'none',
    display: 'inline-block',
    borderRadius: '100%',
    position: 'relative'
  },
  progress : {
    width: '100%',
    '& > * + *': {
      marginTop: theme.spacing(2),
    },
  },
  paper: {
    position: 'relative',
    height: 'auto',
    marginBottom: '10px',
    padding: '10px 8px',
    border: '1px solid #ddd',
    boxSizing: 'border-box',
    marginTop: '20px',
    flexBasis: '100%'
  },
  container: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  left: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
 
  thumbsContainer: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  thumb: {
    display: 'flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    marginBottom: 8,
    marginRight: 8,
    width: 120,
    height: 100,
    padding: 4,
    boxSizing: 'border-box',
  },
  thumbInner: {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden',
  },
  img: {
    display: 'block',
    width: '100%',
    height: '100%'
  },
  remove: {
    position: 'relative',
    top: '-14px',
    width: '0',
    height: '0',
    right: '-100px',
    cursor: 'pointer'
  },
  btnContinue: {
    fontSize: '22px',
    width: '30vw',
    height: '38px',
    textTransform: 'none',
    fontSize: '14px',
    padding: '5px 20px',
    fontWeight: 'bold',
    backgroundColor: '#2ca01c',
    textTransform: 'none',                      
    '&:hover': {
      background: '#2ca01c',
    },
  },
  documentPreview: {
    margin: '15px',
    boxSizing: 'border-box',
  },
  firstDropzone: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%',
    height: 'auto',
    cursor:'pointer'
  },
  loader: {
    position: 'absolute',
    textAlign: 'center',
    left: '47%',
    top: '50%'
  },
  upload: {
    fill: '#2ca01c',
    cursor:'pointer'
  },
  error: {
      color: 'red',
      marginTop: '10px',
      marginBottom: '10px'
  }
  
}));

export default useStyles;
