
/***
 * 
 * Input Field
 * 
 */


import React from 'react';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';

export default function ({ input, autoFocus, type, className, StaticLabel, id, meta: { touched, error, warning } }) {

    const classes = Styles();
    const { name, value, onChange } = input;
    return (
        <div>
            <div className={classes.fieldColor}>
                {StaticLabel && typeof StaticLabel === 'function' && React.createElement(StaticLabel)}
                <TextField
                    multiline
                    name={name}
                    id={id}
                    type={type}
                    rows={6}
                    rowsMax={8}
                    fullWidth
                    onChange={(e) => onChange(e.target.value)}
                    value={value || ''}
                    autoFocus={autoFocus}
                    InputProps={{
                        classes: {
                            input: classes.textSize,
                            padding: 0,
                        }
                    }}
                />
            </div>
            <div className={classes.error}>{touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}</div>
        </div>
    )
}