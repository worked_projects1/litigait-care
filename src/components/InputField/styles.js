

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  textSize: {
    fontSize: '14px',
    padding: 0,
  },
  error: {
    fontSize: '14px',
    color: 'red'
  },
  fieldColor: {
    background: 'rgba(0, 0, 0, 0.09)',
    color: 'black !important',
    height: '150px',
    overflowY: 'auto',
    padding: '10px',
    fontSize: '12px',
    paddingRight: '0px',
    borderBottom: '2px solid #2ca01c',
    cursor: 'pointer'
  },
}));


export default useStyles;