import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({

    btnAccept: {
        fontSize: '18px',
        fontWeight: 'bold',
        backgroundColor: '#2ca01c',
        marginLeft: "12px",
        textTransform: 'none',
        width: '100%',
        marginTop: '10px',
        '&:hover': {
            background: '#2ca01c',
        },
        '&.MuiButton-contained.Mui-disabled': {
            background: '#7bb374',
            color: 'white'
        }
    },
    btnBack: {
        fontSize: '18px',
        fontWeight: 'bold',
        textTransform: 'none',
        width: '100%',
        marginRight: '10px',
        marginTop: '10px',
    },
    btnClear: {
        opacity: "0.5"
    },
    btnSection: {
        marginTop: '12px',
        display: 'flex',
        justifyContent: 'space-between'
    },

    btnGrid: {
        marginTop: '6px',
        marginBottom: '6px'
    },
    fieldColor: {
        '& :after': {
            borderBottomColor: '#2ca01c',
        },
        '& :before': {
            borderBottomColor: '#2ca01c',
        },
        color: 'green !important',
        '& label.Mui-focused': {
            color: '#2ca01c',
        },
        '&.Mui-focused fieldset': {
            borderColor: '#2ca01c',
        },
        '@global': {
          '.MuiInput-formControl': {
            marginTop: '38px !important'
          }
        }
    },
    textSize: {
        fontSize: '20px',
    },
    inputField: {
        borderBottom: '2px solid #2ca01c',
        fontSize: '20px'
    },
    error: {
        fontSize: '14px',
        fontWeight: 'bold',
        color: 'red'
    }

}));


export default useStyles;
