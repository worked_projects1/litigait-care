import React, { useState, useEffect } from 'react';
import { Grid, Typography, FormControlLabel, Button, Checkbox } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import SignatureCanvas from 'react-signature-canvas';
import ClearIcon from '@material-ui/icons/Clear';
import TextField from '@material-ui/core/TextField';
import Styles from './styles';


export default function Signature({ handleSubmit, handleBackPage, TargetLanguageCode, onChangeData }) {
    const classes = Styles()

    const signPad = React.createRef();
    const [width, setWidth] = useState(window.innerWidth);
    const [submitData, setSubmitData] = useState({});
    const theme = useTheme();
    const sm = useMediaQuery(theme.breakpoints.up('sm'));

    useEffect(() => {
        let mounted = true;
        window.addEventListener('resize', () => setWidth(window.innerWidth));
        return () => mounted = false;
    }, []);


    const handleClear = () => {
        signPad.current.clear();
        setSubmitData(Object.assign({}, { ...submitData }, { 'client_signature': false }))
    }

    const handleChange = (e) => {
        const { name, type, value, checked } = e.target;
        
        if(value && value != 'false' || checked){
            setSubmitData(Object.assign({}, { ...submitData }, { [name]: type === 'checked' ? checked : value }));
            onChangeData(Object.assign({}, { ...submitData }, { [name]: type === 'checked' ? checked : value }));
        } else {
            delete submitData[name];
            setSubmitData(Object.assign({}, submitData));
            onChangeData(Object.assign({}, submitData));
        }
        
    }

    return (
        <Grid>
            <Grid item xs={12} style={{ color: "#464444", marginBottom: '12px' }}>
                <Grid container direction="row" justify="space-between">
                    <Grid style={{ display: 'inline-grid', maxWidth: '570px' }}>
                        <Typography variant="inherit">
                            {TargetLanguageCode === 'es' ? 'Dibuja tu firma aquí' : TargetLanguageCode === 'vi' ? 'Vẽ chữ ký của bạn ở đây' : 'Draw your signature here'}
                        </Typography>
                        <Typography variant="inherit" style={{ paddingTop: '10px', paddingBottom: '6px', color: '#000000', fontSize: '13px' }}>
                            {TargetLanguageCode === 'es' ? `Al firmar a continuación, certifica bajo pena de perjurio que las respuestas que ha proporcionado y revisado son verdaderas y precisas según su leal saber y entender.` : TargetLanguageCode === 'vi' ? `Bằng cách ký tên bên dưới, bạn chứng thực theo hình phạt nếu khai man rằng các câu trả lời bạn đã cung cấp và xem xét là đúng và chính xác theo hiểu biết của bạn.` : `By signing below you attest under the penalty of perjury that the answers you've provided and reviewed are true and accurate to the best of your knowledge.`}
                        </Typography>
                    </Grid>
                    <Grid>
                        <Button variant="contained" className={classes.btnClear} startIcon={<ClearIcon />} onClick={handleClear}>
                            {TargetLanguageCode === 'es' ? 'Clara' : TargetLanguageCode === 'vi' ? 'Rỏ ràng' : 'Clear'}
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} style={{marginTop: '12px', marginBottom: '12px'}}>
                <TextField
                    name={"County"}
                    type="text"
                    label={<span className={classes.textSize} > {TargetLanguageCode === 'es' ? `Indique en qué condado se encuentra` : TargetLanguageCode === 'vi' ? `Vui lòng cho biết bạn đang ở quận nào` : `State what county you are in`} </span>}
                    placeholder={TargetLanguageCode === 'es' ? `Condado` : TargetLanguageCode === 'vi' ? `Quận hạt` : `County`}
                    className={classes.fieldColor}
                    fullWidth
                    value={submitData && submitData.County || ''}
                    InputProps={{
                        classes: {
                            input: classes.inputField
                        }
                    }}
                    onChange={handleChange} />
                    {!submitData.County && <span className={classes.error}>{TargetLanguageCode === 'es' ? `Requerida` : TargetLanguageCode === 'vi' ? `Yêu cầu` : `Required` }</span>}
            </Grid>
            <Grid item xs={12} style={{ backgroundColor: "rgb(128 128 128 / 38%)" }}>
                <SignatureCanvas penColor='black'
                    canvasProps={{ width: !sm ? width - 80 : 560, height: 200, className: 'signCanvas' }}
                    ref={signPad}
                    onEnd={(e) => setSubmitData(Object.assign({}, { ...submitData }, { 'client_signature': signPad.current.getTrimmedCanvas().toDataURL('image/png') }))} />
            </Grid>            
            <Grid item xs={12} style={{ marginTop: "25px", marginBottom: "15px" }} >
                <FormControlLabel
                    name="Terms"
                    control={<Checkbox style={{ color: "green" }}
                        value={submitData && submitData.Terms || false}
                        onChange={handleChange} />}
                    label={TargetLanguageCode === 'es' ? `Acepto utilizar registros y firmas electrónicos` : TargetLanguageCode === 'vi' ? `Tôi đồng ý sử dụng hồ sơ và chữ ký điện tử ` : `I agree to use electronic records and signatures`}
                />
            </Grid>
            <Grid container direction="row" style={sm ? null : { flexWrap: 'initial' }} justify="flex-end" alignItems="center" className={classes.btnGrid}>
                <Button type="button" variant="contained" className={classes.btnBack} style={sm ? { width: 150 } : null} onClick={handleBackPage} >{TargetLanguageCode === 'es' ? 'atrás' : TargetLanguageCode === 'vi' ? 'Lưng' : 'Back'}</Button>
                <Button type="button" className={classes.btnAccept} style={sm ? { width: 250 } : null} disabled={submitData && submitData.client_signature && submitData.Terms && submitData.County ? false : true} variant="contained" color="primary" onClick={() => handleSubmit(submitData)}>{TargetLanguageCode === 'es' ? 'Aceptar' : TargetLanguageCode === 'vi' ? 'Chấp nhận' : 'Accept'}</Button>
            </Grid>
        </Grid>
    )
}
