/**
 * 
 * Alert Dialog
 * 
 */

import React, { useState } from 'react';
import { Grid, Typography, Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import Styles from './styles';

export default function AlertDialog({ title, children, btnLabel1, btnLabel2, description, onConfirm, onConfirmPopUpClose, openDialog, closeDialog, heading, footerBtn, header }) {
  
  const classes = Styles();
  const [open, setOpen] = useState(false);

  const close = () => {
    setOpen(false);
    if (closeDialog) {
      closeDialog();
    }
  }

  const confirm = () => {
    onConfirm();
    if (onConfirmPopUpClose) {
      close();
    }
  }


  return (
    <div>
      {children && children(() => setOpen(!open))}
      <Dialog
        open={open || openDialog || false}
        onClose={close}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        {(title || header) && <DialogTitle id="alert-dialog-title" >{title}</DialogTitle>}
        {typeof heading === 'function' && heading() || heading && <Grid className={classes.gridHeader}>
            <Typography variant="h6" gutterBottom component="span"><b>{heading || ''}</b></Typography>
        </Grid> || null}
        {description && <DialogContent>
           <DialogContentText id="alert-dialog-description" className={classes.description}>
            <span dangerouslySetInnerHTML={{ __html: description }} />
          </DialogContentText>
        </DialogContent>}
        <DialogActions>
          <Button onClick={close} color="primary" className={classes.button}>
            {btnLabel2}
          </Button>
          {footerBtn && typeof footerBtn === 'function' ? React.createElement(footerBtn) : btnLabel1 ? <Button onClick={confirm} color="primary" autoFocus className={classes.button}>
            {btnLabel1}
          </Button> : null}
        </DialogActions>
      </Dialog>
    </div>);
}