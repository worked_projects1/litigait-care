/**
 * 
 * PDF Viewer
 * 
 */

import React from 'react'
import PDFViewer from 'pdf-viewer-reactjs'
import { Grid } from '@material-ui/core';
import styles from './styles';

function PDFViewers(props){

    const { base64 } = props;
    const classes = styles()

    return (<Grid container item spacing={3} justify="center" className={classes.root}>  
        <Grid className={classes.viewer}>      
            <PDFViewer 
                document={{
                    base64: base64 || window.wbase_64
                }}
                hideNavbar={true}
            />
        </Grid>
    </Grid>)
}

export default PDFViewers
