import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    root: {
        margin: '0px',
        width: '100%',
        backgroundColor: "rgb(82, 86, 89)",
        paddingTop: "67px"
    },
    viewer: { 
        maxWidth: '600px', 
        width: '100%' 
    }
}));


export default useStyles;