/**
 * 
 * PDF Maker
 * 
 */


import React from 'react';
import { CreatePDF } from './utils';
import { Grid } from '@material-ui/core';
import styles from './styles';


function PDFMaker(props) {
    
    const { className, style, children  } = props;
    const classes = styles();

    return (<Grid className={`${className} ${classes.PDFMaker}`} style={style}>
        {children && children(() => CreatePDF(props))}
    </Grid>)
}

export default PDFMaker;