/**
 * 
 * Utils
 * 
 */

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

pdfMake.fonts = {
    Roboto: {
      normal: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Regular.ttf',
      bold: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Medium.ttf',
      italics: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-Italic.ttf',
      bolditalics: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.66/fonts/Roboto/Roboto-MediumItalic.ttf'
    }
 }


/**
 * 
 * @param {object} params 
 */
export function CreatePDF(params) {

    const { type, docDefinition, callback } = params;
    const pdfDocGenerator = pdfMake.createPdf(docDefinition);

    if (type === '_self') {
        //Open the PDF in a new window
        pdfDocGenerator.print({}, window);
    } else if (type === '_blank') {
        //Open the PDF in a same window
        pdfDocGenerator.open();
    } else if (type === '_print') {
        //Print the PDF
        pdfDocGenerator.print()
    } else if (type === '_base64') {
        //Get the PDF as base64 data
        pdfDocGenerator.getBase64((data) => {
            if (callback)
                callback(data);
        });
    } else if (type === '_buffer') {
        //Get the PDF as buffer
        pdfDocGenerator.getBuffer((buffer) => {
            if (callback)
                callback(buffer);
        });
    } else if (type === '_blob') {
        //Get the PDF as Blob
        pdfDocGenerator.getBlob((blob) => {
            if (callback)
                callback(blob);
        });
    } else if (type === '_stream') {
        //Get PDFKit document object as stream
        pdfDocGenerator.getStream((document) => {
            if (callback)
                callback(document);
        });
    } else if (type === '_url') {
        //Get PDFKit document object as stream
        pdfDocGenerator.getDataUrl((document) => {
            if (callback)
                callback(document);
        });
    }
}