import axios from 'axios';

const api = axios.create({
  baseURL: process.env.NODE_ENV === 'development' ? 'http://localhost:3000/test/' : 'https://staging-api.esquiretek.com/',
  timeout: 40000,
  headers: { Accept: 'application/json' },
});

export function setAuthToken(authToken) {
  api.defaults.headers.common['Authorization'] = authToken;
}

export default api;


export function uploadFile(uploadUrl, data, contentType) {
  return axios.put(uploadUrl, data, {
    headers: {
      Accept: 'application/json',
      'Content-Type': contentType,
    },
  }).then((response) => response.data);
}


export function getSignature(file_name, content_type) {
  return api.post(`/rest/forms/upload-file`, Object.assign({}, { file_name, content_type })).then((response) => response.data).catch((error) => Promise.reject(error));
}
